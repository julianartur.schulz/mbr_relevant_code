class ImageAcquirer:
    """
    Manages everything you need to acquire images from the connecting device.
    """
    _event = Event()
    _specialized_tl_type = ['U3V', 'GEV']

    _supported_parameters = [
        ParameterKey.ENABLE_CLEANING_UP_INTERMEDIATE_FILES,
        ParameterKey.ENABLE_AUTO_CHUNK_DATA_UPDATE,
        ParameterKey.REMOTE_DEVICE_SOURCE_XML_FILE_PATH,
        ParameterKey.THREAD_SLEEP_PERIOD,
        ParameterKey.TIMEOUT_PERIOD_ON_UPDATE_EVENT_DATA_CALL,
        ParameterKey.TIMEOUT_PERIOD_ON_CLIENT_FETCH_CALL,
        ParameterKey.NUM_BUFFERS_FOR_FETCH_CALL,
    ]

    class Events(IntEnum):
        __doc__ = "Possible events that the " \
                  ":class:`~harvesters.core.ImageAcquirer` class " \
                  "can notify to the client."
        TURNED_OBSOLETE = 0  # doc: The :class:`~ImageAcquirer` obejct has turned obsolete."
        NEW_BUFFER_AVAILABLE = 1  # doc: A buffer has turned available to be fetched.
        RETURN_ALL_BORROWED_BUFFERS = 2  # doc: Notifies that the fetched buffers must be queued.
        READY_TO_STOP_ACQUISITION = 3  # doc: Notifies the ongoing image acquisition process can be stopped.
        INCOMPLETE_BUFFER = 4  # doc: Notifies a buffer has been fetched but it was incomplete and unusable.
        ON_CHUNK_DATA_UPDATED = 5
        ON_EVENT_DATA_UPDATED = 6

    def _create_acquisition_thread(self) -> _ImageAcquisitionThread:
        return _ImageAcquisitionThread(image_acquire=self)

    def __init__(self, *, parent: Harvester, device_proxy=None, config: Optional[ParameterSet] = None, profiler=None, file_dict=None):
        """

        Parameters
        ----------
        device_proxy : Device_
        sleep_duration : float
        file_path : str
            Set a path to camera description file which you want to load on
            the target node map instead of the one which the device declares.
        update_chunk_automatically : bool
            Set :const:`True` if you want to let the chunk data being updated
            when a buffer is newly fetched. :const:`False` means you will
            explicitly update the chunk data by yourself when needed.
        file_dict : Dict[str, bytes]
        """
        global _logger
        ParameterSet.check(config, self._supported_parameters)
        super().__init__()

        self._parent = parent
        self._is_valid = True
        self._file_dict = file_dict
        self._clean_up = ParameterSet.get(ParameterKey.ENABLE_CLEANING_UP_INTERMEDIATE_FILES, True, config)
        self._enable_auto_chunk_data_update = ParameterSet.get(ParameterKey.ENABLE_AUTO_CHUNK_DATA_UPDATE, True, config)
        self._has_attached_chunk = False

        env_var = 'HARVESTERS_XML_FILE_DIR'
        if env_var in os.environ:
            self._xml_dir = os.getenv(env_var)
        else:
            self._xml_dir = None

        self._device_proxy = device_proxy

        self._thread_factory_method_for_event_module = ParameterSet.get(ParameterKey.THREAD_FACTORY_METHOD_FOR_EVENT_MODULE, lambda: _EventMonitor(parent=self), config)
        file_path = ParameterSet.get(ParameterKey.REMOTE_DEVICE_SOURCE_XML_FILE_PATH, None, config)

        self._remote_device = RemoteDevice(
            module=device_proxy.module, parent=device_proxy, file_path=file_path,
            file_dict=file_dict, do_clean_up=self._clean_up,
            xml_dir_to_store=self._xml_dir)
        self._interface_proxy = device_proxy.parent
        self._system_proxy = self._interface_proxy.parent

        if self._remote_device:
            assert self._remote_device.node_map

        self._data_streams = []

        self._new_buffer_event_monitor_dict = dict()

        self._module_event_monitor_dict = dict()
        self._module_event_monitor_thread_dict = dict()

        modules = [self._system_proxy,
                   self._interface_proxy,
                   self._device_proxy,
                   self._remote_device]
        event_types = [EVENT_TYPE_LIST.EVENT_MODULE,
                       EVENT_TYPE_LIST.EVENT_MODULE,
                       EVENT_TYPE_LIST.EVENT_MODULE,
                       EVENT_TYPE_LIST.EVENT_REMOTE_DEVICE]
        event_managers = [EventManagerModule,
                          EventManagerModule,
                          EventManagerModule,
                          EventManagerRemoteDevice]

        for module, event_type, manager in \
                zip(modules, event_types, event_managers):
            try:
                self._module_event_monitor_dict[module] = \
                    manager(module.register_event(event_type))
            except NotImplementedException:
                _logger.debug("no module event: {}".format(module))
            except ResourceInUseException:
                _logger.debug("resource in use: {}".format(module))
            else:
                self._module_event_monitor_thread_dict[module] = \
                    self._thread_factory_method_for_event_module()
                self._module_event_monitor_thread_dict[module].worker = \
                    self._worker_module_event

        self._create_ds_at_connection = True
        if self._create_ds_at_connection:
            self._setup_data_streams()

        self._profiler = profiler

        self._num_buffers_to_hold = 1
        self._queue = Queue(maxsize=self._num_buffers_to_hold)

        self._thread_factory_method_for_event_new_buffer = ParameterSet.get(
            ParameterKey.THREAD_FACTORY_METHOD,
            lambda: _EventMonitor(parent=self), config)

        self._event_new_buffer_thread = \
            self._thread_factory_method_for_event_new_buffer()
        self._event_new_buffer_thread.worker = self._worker_event_new_buffer

        if current_thread() is main_thread():
            self._sigint_handler = _SignalHandler(
                event=self._event, subject=self)
            signal.signal(signal.SIGINT, self._sigint_handler)
            _logger.debug('created: {0}'.format(self._sigint_handler))

        self._num_images_to_acquire = 0

        self._timeout_on_internal_fetch_call = ParameterSet.get(ParameterKey.TIMEOUT_PERIOD_ON_UPDATE_EVENT_DATA_CALL, 1, config)  # ms
        self._timeout_on_client_fetch_call = ParameterSet.get(ParameterKey.TIMEOUT_PERIOD_ON_CLIENT_FETCH_CALL, 0.01, config)  # s

        self._statistics = Statistics()
        self._announced_buffers = []

        self._has_acquired_1st_image = False
        self._is_acquiring = False

        num_buffers_default = ParameterSet.get(ParameterKey.NUM_BUFFERS_FOR_FETCH_CALL, 3, config)
        try:
            self._min_num_buffers = self._data_streams[0].buffer_announce_min
        except GenTL_GenericException as e:
            # In general, a GenTL Producer should not raise the
            # InvalidParameterException to the inquiry for
            # STREAM_INFO_BUF_ANNOUNCE_MIN because it is totally legal
            # but we have observed a fact that there is at least one on
            # the market. As a workaround we involve this try-except block:
            _logger.warning(e, exc_info=True)
            self._min_num_buffers = num_buffers_default
            self._num_buffers = num_buffers_default
        else:
            self._num_buffers = max(
                num_buffers_default, self._min_num_buffers)

        tl_type = self.device.tl_type
        self._chunk_adapter = self._get_chunk_adapter(
            tl_type=tl_type, node_map=self.remote_device.node_map)
        self._event_adapter = self._get_event_adapter(
            tl_type=tl_type, node_map=self.remote_device.node_map)

        self._finalizer = weakref.finalize(self, self.destroy)

        self._supported_events = [
            self.Events.TURNED_OBSOLETE,
            self.Events.RETURN_ALL_BORROWED_BUFFERS,
            self.Events.READY_TO_STOP_ACQUISITION,
            self.Events.NEW_BUFFER_AVAILABLE,
            self.Events.INCOMPLETE_BUFFER,
            self.Events.ON_CHUNK_DATA_UPDATED,
            self.Events.ON_EVENT_DATA_UPDATED,
        ]
        self._callback_dict = dict()
        for event in self._supported_events:
            self._callback_dict[event] = None

        self._enable_event_monitor = ParameterSet.get(
            ParameterKey.ENABLE_EVENT_MONITOR, False, config)

        if self._enable_event_monitor:
            for thread in self._module_event_monitor_thread_dict.values():
                thread.start()

    def is_valid(self) -> bool:
        """
        bool: Returns :const:`False` and it means the object has been
        obsolete and there is nothing to do for you. Just leave the object
        and start working with another newly created object.

        The :meth:`Harvester.update` method call on its parent
        :class:`Harvester` object make its owning :class:`ImageAcquirer`
        objects obsolete as the available device information is completely
        reconstructed.
        """
        return self._is_valid

    def _emit_callbacks(self, event: Events) -> None:
        global _logger

        callbacks = self._callback_dict[event]
        if isinstance(callbacks, Iterable):
            for callback in callbacks:
                self._emit_callback(callback)
        else:
            callback = callbacks
            self._emit_callback(callback)

    def _emit_callback(
            self,
            callback: Optional[Union[Callback, List[Callback]]]) -> None:
        if callback:
            if isinstance(callback, Callback):
                _logger.debug("going to emit: {0}".format(callback))
                callback.emit(context=self)
            else:
                raise TypeError

    def _check_validity(self, event: Events):
        if event not in self._supported_events:
            raise ValueError

    def add_callback(self, event: Events, callback: Callback):
        self._check_validity(event)
        assert callback
        self._callback_dict[event] = callback

    def remove_callback(self, event: Events):
        self._check_validity(event)
        self._callback_dict[event] = None

    def remove_callbacks(self):
        for event in self._supported_events:
            self._callback_dict[event] = None

    @property
    def supported_events(self):
        return self._supported_events

    @staticmethod
    def _get_chunk_adapter(*, tl_type: str, node_map: NodeMap):
        if tl_type == 'U3V':
            return ChunkAdapterU3V(node_map)
        elif tl_type == 'GEV':
            return ChunkAdapterGEV(node_map)
        else:
            return ChunkAdapterGeneric(node_map)

    @staticmethod
    def _get_event_adapter(*, tl_type: str, node_map: NodeMap):
        if tl_type == 'U3V':
            return EventAdapterU3V(node_map.pointer)
        elif tl_type == 'GEV':
            return EventAdapterGEV(node_map.pointer)
        else:
            return EventAdapterGeneric(node_map.pointer)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._finalizer()

    def _release_remote_device(self):
        self._remote_device.deregister_node_callbacks()

        if self.remote_device.node_map:
            self.remote_device.node_map.disconnect()

        if self._chunk_adapter:
            self._chunk_adapter = None

        if self._device_proxy.is_open():
            self._device_proxy.close()

    def destroy(self) -> None:
        """
        Destroys itself; releases all preserved external resources such as
        buffers or the connected remote device.

        Note that once the destroy method is called on the object then the
        object will immediately turn obsolete and there will never be any
        way to make the object usable. Just throw the object away and
        create another object by calling
        :meth:`Harvester.create_image_acquire` method.
        """
        global _logger
        _logger.info('going to release resources: {}'.format(self))

        if not self._is_valid:
            return

        for thread in self._module_event_monitor_thread_dict.values():
            if thread.is_running():
                thread.stop()
                thread.join()

        self.stop()

        if self._event_adapter:
            self._event_adapter = None

        if self._chunk_adapter:
            self._chunk_adapter = None

        self._release_data_streams()
        self._release_remote_device()

        self._new_buffer_event_monitor_dict.clear()
        self._module_event_monitor_dict.clear()
        self._module_event_monitor_thread_dict.clear()

        self._remote_device = None
        self._device_proxy = None
        self._interface_proxy = None
        self._system_proxy = None

        self._is_valid = False

        if self._profiler:
            self._profiler.print_diff()

        _logger.info('released resources: {}'.format(self))
        self._emit_callbacks(self.Events.TURNED_OBSOLETE)

    @property
    def num_buffers(self) -> int:
        """
        int: The number of buffers that is prepared for the image acquisition
        process. The buffers will be announced to the target GenTL Producer.
        """
        return self._num_buffers

    @num_buffers.setter
    def num_buffers(self, value: int = 1):
        if value >= self._min_num_buffers:
            self._num_buffers = value
        else:
            raise ValueError(
                'The number of buffers must be '
                'greater than or equal to {}'.format(self._min_num_buffers))

    @property
    def min_num_buffers(self) -> int:
        """
        int: The minimum number of the buffers for image acquisition. You
        have to set a value to :meth:`num_buffers` so that is greater than or
        equal to this.
        """
        return self._min_num_buffers

    @property
    def num_filled_buffers_to_hold(self) -> int:
        """
        int: The number of buffers that is used for a case where the image
        acquisition process runs in the background. You will fetch buffers
        from the buffers when you call the :meth:`fetch` method in a
        case you started the image acquisition passing :const:`True` to
        :data:`run_as_thread` of the :meth:`start` method.
        """
        return self._num_buffers_to_hold

    @num_filled_buffers_to_hold.setter
    def num_filled_buffers_to_hold(self, value: int = 1):
        global _logger

        if value > 0:
            self._num_buffers_to_hold = value

            buffers = []
            while not self._queue.empty():
                buffers.append(self._queue.get_nowait())

            self._queue = Queue(maxsize=self._num_buffers_to_hold)

            while len(buffers) > 0:
                try:
                    self._queue.put(buffers.pop(0))
                except Full as e:
                    _logger.debug(e, exc_info=True)
                    buffer = buffers.pop(0)
                    buffer.parent.queue_buffer(buffer)

        else:
            raise ValueError(
                'The number of filled buffers to hold must be > 0.')

    @property
    def num_holding_filled_buffers(self) -> int:
        """
        int: The number of available buffers, i.e., the buffers that contain
        images.
        """
        return self._queue.qsize()

    @property
    def data_streams(self) -> List[DataStream]:
        """
        List[DataStream]: A list of GenTL :class:`DataStream` objects that the
        :class:`ImageAcquire` object is working with.
        """
        return self._data_streams

    @property
    def remote_device(self) -> RemoteDevice:
        """
        RemoteDevice: The remote GenTL :class:`Device` object, typically a
        camera, that the :class:`ImageAcquire` object is working with.
        """
        return self._remote_device

    @property
    def device(self) -> Device:
        """
        Device: The local GenTL :class:`Device` proxy object that the
        :class:`ImageAcquire` object is working with.
        """
        return self._device_proxy

    @property
    def interface(self) -> Interface:
        """
        Interface: The GenTL :class:`Interface` object that the
        :class:`ImageAcquire` object is working with.
        """
        return self._interface_proxy

    @property
    def system(self) -> System:
        """
        System: The GenTL :class:`System` object that the
        :class:`ImageAcquire` object is working with.
        """
        return self._system_proxy

    def is_acquiring_images(self):
        """
        Attention
        ---------
            It has been deprecated at 1.3.7 and will be removed in 2.0.0.
            It will be replaced by :meth:`is_acquiring`.
        """
        _indicate_deprecation(self.is_acquiring_images, self.is_acquiring)
        return self.is_acquiring()

    def is_acquiring(self) -> bool:
        """
        Returns the truth value of a proposition: It's acquiring images.

        Returns
        -------
        bool
            :const:`True` if it's acquiring images. Otherwise :const:`False`.
        """
        return self._is_acquiring

    def is_armed(self) -> bool:
        """
        Returns the arm status on image acquisition.

        Returns
        -------
            :const:`True` if it's been armed for image acquisition; otherwise,
            :const:`False`.
        """
        if not self.is_acquiring() or \
                self.is_acquiring() and self._num_images_to_acquire == 0:
            return False
        else:
            return True

    @property
    def timeout_on_client_fetch_call(self) -> float:
        """
        Attention
        ---------
            It has been deprecated at 1.3.6 and will be removed in 2.0.0.
            It will be replaced by :attr:`timeout_period_on_client_fetch_call`.
        """
        _indicate_deprecation('timeout_on_client_fetch_call', 'timeout_period_on_client_fetch_call')
        return self.timeout_period_on_client_fetch_call

    @timeout_on_client_fetch_call.setter
    def timeout_on_client_fetch_call(self, value: float):
        _indicate_deprecation('timeout_on_client_fetch_call', 'timeout_period_on_client_fetch_call')
        self.timeout_period_on_client_fetch_call = value

    @property
    def timeout_period_on_client_fetch_call(self) -> float:
        """
        float: It is used to define the timeout duration on a single fetch
        method calL. The unit is [s].
        """
        return self._timeout_on_client_fetch_call

    @timeout_period_on_client_fetch_call.setter
    def timeout_period_on_client_fetch_call(self, value: float):
        client = value
        internal = float(self.timeout_period_on_update_event_data_call / 1000)
        info = json.dumps({"internal": internal, "client": client})
        if isclose(client, internal):
            _logger.warning("may cause timeout: {}".format(info))
        else:
            if client < internal:
                _logger.warning("may cause timeout: {}".format(info))
        self._timeout_on_client_fetch_call = value

    @property
    def timeout_period_on_update_event_data_call(self) -> int:
        """
        int: It is used to define the timeout duration on an internal single
        GenTL fetch calL. The unit is [ms].
        """
        return self._timeout_on_internal_fetch_call

    @timeout_period_on_update_event_data_call.setter
    def timeout_period_on_update_event_data_call(self, value):
        internal = float(value)
        client = self.timeout_period_on_client_fetch_call * 1000.
        info = json.dumps({"internal": internal, "client": client})
        if isclose(internal, client):
            _logger.warning("may cause timeout: {}".format(info))
        else:
            if internal > client:
                _logger.warning("may cause timeout: {}".format(info))
        self._timeout_on_internal_fetch_call = value

    @property
    def timeout_for_image_acquisition(self) -> int:
        """
        Attention
        ---------
            It has been deprecated at 1.3.6 and will be removed in 2.0.0.
            It will be replaced by :attr:`timeout_period_on_update_event_data_call`.
        """
        _indicate_deprecation('timeout_for_image_acquisition', 'timeout_period_on_update_event_data_call')
        return self._timeout_on_internal_fetch_call

    @timeout_for_image_acquisition.setter
    def timeout_for_image_acquisition(self, ms):
        _indicate_deprecation('timeout_for_image_acquisition', 'timeout_period_on_update_event_data_call')
        self._timeout_on_internal_fetch_call = ms

    @property
    def thread_image_acquisition(self) -> ThreadAdapter:
        """
        Attention
        ---------
            It has been deprecated at 1.3.6 and will be removed in 2.0.0.
        """
        return self._thread_image_acquisition

    @thread_image_acquisition.setter
    def thread_image_acquisition(self, obj):
        self._thread_image_acquisition = obj
        self._thread_image_acquisition.worker = self.worker_image_acquisition

    @property
    def statistics(self) -> Statistics:
        """
        Statistics: The statistics about image acquisition.
        """
        return self._statistics

    def _setup_data_streams(self, file_dict: Dict[str, bytes] = None):
        global _logger

        for i, stream_id in enumerate(self._device_proxy.data_stream_ids):
            _data_stream = self._device_proxy.create_data_stream()

            try:
                _data_stream.open(stream_id)
            except GenTL_GenericException as e:
                _logger.error(e, exc_info=True)
            else:
                _logger.debug(
                    'opened: {0}'.format(_family_tree(_data_stream)))

            self._data_streams.append(DataStream(module=_data_stream))

            self._new_buffer_event_monitor_dict[self._data_streams[i]] = \
                EventManagerNewBuffer(
                    self._data_streams[i].register_event(
                        EVENT_TYPE_LIST.EVENT_NEW_BUFFER))

    def start_image_acquisition(self, run_in_background=False):
        """
        Attention
        ---------
            It has been deprecated at 1.3.6 and will be removed at 2.0.0.
            It will be replaced by :meth:`start`.
        """
        _indicate_deprecation(self.start_image_acquisition, self.start_acquisition)
        self.start_acquisition(run_in_background=run_in_background)

    def start_acquisition(self, run_in_background: bool = False) -> None:
        """
        Attention
        ---------
            It has been deprecated at 1.3.7 and will be removed at 2.0.0.
            It will be replaced by :meth:`start`.
        """
        _indicate_deprecation(self.start_acquisition, self.start)
        self.start(run_as_thread=run_in_background)

    def start(self, *, run_as_thread: bool = False) -> None:
        """
        Starts image acquisition process.

        Parameters
        ----------
        run_as_thread : bool
            Set :const:`True` if you want to let the ImageAcquire object keep
            acquiring images in the background and the images you get calling
            :meth:`fetch` method will be from the :class:`ImageAcquirer`.
            Otherwise, the images will directly come from the target GenTL
            Producer.
        """
        global _logger

        if not self.is_acquiring():
            self._num_images_to_acquire = 0

        # Refill the number of images to be acquired if needed:
        if self._num_images_to_acquire == 0:
            # In this case, the all images have been acquired from the
            # previous session; now we refill the number of the images
            # to acquire in the next session:
            try:
                acq_mode = self.remote_device.node_map.AcquisitionMode.value
            except GenTL_GenericException as e:
                num_images_to_acquire = -1
                _logger.warning(e, exc_info=True)
            else:
                if acq_mode == 'Continuous':
                    num_images_to_acquire = -1
                elif acq_mode == 'SingleFrame':
                    num_images_to_acquire = 1
                elif acq_mode == 'MultiFrame':
                    num_images_to_acquire = \
                        self.remote_device.node_map.AcquisitionFrameCount.value
                else:
                    num_images_to_acquire = -1

            self._num_images_to_acquire = num_images_to_acquire

        if not self.is_armed():
            # In this case, we have not armed our GenTL Producer yet for
            # the coming image acquisition; let's arm it:

            if not self._create_ds_at_connection:
                self._setup_data_streams(file_dict=self._file_dict)

            for ds in self._data_streams:
                if ds.defines_payload_size():
                    buffer_size = ds.payload_size
                else:
                    buffer_size = self.remote_device.node_map.PayloadSize.value

                num_required_buffers = self._num_buffers
                try:
                    num_buffers = ds.buffer_announce_min
                    if num_buffers < num_required_buffers:
                        num_buffers = num_required_buffers
                except GenTL_GenericException as e:
                    num_buffers = num_required_buffers
                    _logger.warning(e, exc_info=True)

                num_buffers = max(num_buffers, self._num_images_to_acquire)

                raw_buffers = self._create_raw_buffers(num_buffers, buffer_size)
                buffer_tokens = self._create_buffer_tokens(raw_buffers)
                self._announced_buffers = self._announce_buffers(
                    data_stream=ds, _buffer_tokens=buffer_tokens)
                self._queue_announced_buffers(
                    data_stream=ds, buffers=self._announced_buffers)

                try:
                    self.remote_device.node_map.TLParamsLocked.value = 1
                except GenTL_GenericException:
                    # SFNC < 2.0
                    pass
                except AttributeError:
                    _logger.debug("no TLParamsLocked: {}".format(
                        _family_tree(self._device_proxy.module)))

                ds.start_acquisition(
                    ACQ_START_FLAGS_LIST.ACQ_START_FLAGS_DEFAULT, -1)

            self._has_attached_chunk = False
            self._is_acquiring = True

            if run_as_thread:
                if self._event_new_buffer_thread:
                    self._event_new_buffer_thread.start()

        _logger.info('started acquisition: {0}'.format(self))

        self.remote_device.node_map.AcquisitionStart.execute()
        _logger.debug('started streaming: {0}'.format(
                _family_tree(self._device_proxy.module)))

        if self._profiler:
            self._profiler.print_diff()

    def _worker_module_event(self) -> None:
        """
        The worker method of the image acquisition task.

        :return: None
        """
        global _logger
        for monitor in self._module_event_monitor_dict.values():
            if not self._is_valid or not self._event_adapter:
                return

            try:
                monitor.update_event_data(self.timeout_period_on_update_event_data_call)
            except TimeoutException:
                continue
            except GenTL_GenericException as e:
                _logger.debug(e, exc_info=True)
            else:
                _logger.debug("going to deliver an event: {}".format(monitor))
                if self._device_proxy.tl_type in self._specialized_tl_type:
                    self._event_adapter.deliver_message(monitor.optional_data)
                else:
                    self._event_adapter.deliver_message(monitor.optional_data,
                                                        monitor.event_id)
                self._emit_callbacks(self.Events.ON_EVENT_DATA_UPDATED)
                _logger.debug("just delivered an event: {}".format(monitor))

    def _worker_event_new_buffer(self) -> None:
        """
        The worker method of the image acquisition task.

        :return: None
        """
        global _logger

        queue = self._queue

        for monitor in self._new_buffer_event_monitor_dict.values():
            buffer = self._fetch(monitor=monitor,
                                 timeout_on_client_fetch_call=self.timeout_on_client_fetch_call)
            if buffer:
                with MutexLocker(self._event_new_buffer_thread):
                    if not self._is_acquiring:
                        return
                    if queue.full():
                        _buffer = queue.get()
                        _buffer.parent.queue_buffer(_buffer)
                    queue.put(buffer)
                self._emit_callbacks(self.Events.NEW_BUFFER_AVAILABLE)

    def _update_chunk_data(self, *, buffer: _Buffer, is_manual: bool):
        global _logger

        try:
            if not buffer.is_containing_chunk_data():
                return
        except GenTL_GenericException:
            try:
                if buffer.num_chunks == 0:
                    return
            except GenTL_GenericException:
                if _is_logging_buffer:
                    _logger.warning(
                        'no way to check chunk availability: {0}'.format(
                            _family_tree(buffer)))
                return
            else:
                if _is_logging_buffer:
                    _logger.debug('contains chunk data: {0}'.format(
                        _family_tree(buffer)))

        if buffer.tl_type not in self._specialized_tl_type:
            try:
                self._chunk_adapter.attach_buffer(
                    buffer.raw_buffer,
                    buffer.chunk_data_info_list)
            except GenTL_GenericException as e:
                _logger.error(e, exc_info=True)
        else:
            try:
                size = buffer.delivered_chunk_payload_size
            except GenTL_GenericException:
                try:
                    size = buffer.size_filled
                except GenTL_GenericException:
                    size = len(buffer.raw_buffer)

            if self._has_attached_chunk:
                self._chunk_adapter.update_buffer(buffer.raw_buffer)
                action = 'updated'
            else:
                self._chunk_adapter.attach_buffer(buffer.raw_buffer, size)
                self._has_attached_chunk = True
                action = 'attached'

            if _is_logging_buffer:
                _logger.debug('chunk data: {}, {}'.format(
                    action, _family_tree(buffer)))

            if not is_manual:
                self._emit_callbacks(self.Events.ON_CHUNK_DATA_UPDATED)

    def try_fetch(self, *, timeout: float,
                  is_raw: bool = False) -> Union[Buffer, _Buffer, None]:
        """
        Unlike the fetch method, the try_fetch method gives up and
        returns None if no complete buffer was acquired during the defined
        period.

        Parameters
        ----------
        timeout : float
            Set the period that defines the expiration for an available
            buffer delivery; if no buffer is fetched within the period
            then None will be returned. The unit is [s].

        is_raw : bool
            Set :const:`True` if you need a raw GenTL Buffer module; note
            that you'll have to manipulate the object by yourself.

        Returns
        -------
        Union[Buffer, _Buffer, None]
            A buffer if it is complete; otherwise None.
        """
        buffers = []
        for monitor in self._new_buffer_event_monitor_dict.values():
            buffer = self._fetch(monitor=monitor,
                                 timeout_on_client_fetch_call=timeout,
                                 throw_except=False)

            buffers.append(self._finalize_fetching_process(buffer, is_raw))

        return buffers if len(self._new_buffer_event_monitor_dict.values()) > 1 else buffers[0]

    def _fetch(self, *, monitor: EventManagerNewBuffer,
               timeout_on_client_fetch_call: float = 0,
               throw_except: bool = False) -> Union[Buffer, _Buffer, None]:
        global _logger

        assert monitor

        buffer = None
        watch_timeout = True if timeout_on_client_fetch_call > 0 else False
        base = time.time()

        while not buffer:
            if watch_timeout:
                elapsed = time.time() - base
                if elapsed > timeout_on_client_fetch_call:
                    if _is_logging_buffer:
                        _logger.debug(
                            'timeout: elapsed {0} sec.'.format(
                                timeout_on_client_fetch_call))
                    if throw_except:
                        raise TimeoutException
                    else:
                        return None

            try:
                monitor.update_event_data(self.timeout_period_on_update_event_data_call)
            except TimeoutException:
                continue
            except GenTL_GenericException as e:
                _logger.error(e, exc_info=True)
                raise
            else:
                context = None
                frame_id = None
                try:
                    is_complete = monitor.buffer.is_complete()
                    if _is_logging_buffer:
                        context = monitor.buffer.context
                        frame_id = monitor.buffer.frame_id
                except GenTL_GenericException:
                    is_complete = False

                if is_complete:
                    self._update_num_images_to_acquire()
                    self._update_statistics(monitor.buffer)
                    buffer = monitor.buffer
                    if _is_logging_buffer:
                        _logger.debug(
                            'fetched: {0} (#{1}); {2}'.format(
                                context, frame_id,
                                _family_tree(monitor.buffer)))
                else:
                    _logger.warning(
                        'incomplete or not available; discarded: {}'.format(
                            _family_tree(monitor.buffer)))

                    ds = monitor.buffer.parent
                    ds.queue_buffer(monitor.buffer)
                    self._emit_callbacks(self.Events.INCOMPLETE_BUFFER)
                    return None

        return buffer

    def _try_fetch_from_queue(
            self, *, is_raw: bool = False) -> Union[Buffer, _Buffer, None]:
        with MutexLocker(self._event_new_buffer_thread):
            try:
                raw_buffer = self._queue.get(block=False)
                return self._finalize_fetching_process(raw_buffer, is_raw)
            except Empty:
                return None

    def _finalize_fetching_process(
            self, raw_buffer: _Buffer, is_raw: bool) -> Union[Buffer, _Buffer, None]:
        if not raw_buffer:
            return None

        if self._enable_auto_chunk_data_update:
            self._update_chunk_data(buffer=raw_buffer, is_manual=False)

        if is_raw:
            return raw_buffer

        try:
            buffer = Buffer(module=raw_buffer,
                            node_map=self.remote_device.node_map,
                            acquire=self)
        except GenTL_GenericException:
            family_tree = _family_tree(raw_buffer)
            _logger.warning(
                'information not available; discarded: {}'.format(
                    family_tree))
            raw_buffer.parent.queue_buffer(raw_buffer)
            return None
        else:
            return buffer

    def fetch_buffer(self, *, timeout: float = 0, is_raw: bool = False,
                     cycle_s: float = None) -> Buffer:
        """
        Attention
        ---------
            It has been deprecated at 1.3.6 and will be removed at 2.0.0.
            It will be replaced by :meth:`fetch`.
        """
        _indicate_deprecation(self.fetch_buffer, self.fetch)
        return self.fetch(timeout=timeout, is_raw=is_raw, cycle_s=cycle_s)

    def fetch(self, *, timeout: float = 0, is_raw: bool = False,
                     cycle_s: float = None) -> Union[Buffer, _Buffer, None]:
        """
        Fetches an available :class:`Buffer` object that has been filled up
        with a single image and returns it.

        Parameters
        ----------
        timeout : float
            Set the period that defines the expiration for an available
            buffer delivery; if no buffer is fetched within the period
            then TimeoutException will be raised. The unit is [s].

        is_raw : bool
            Set :const:`True` if you need a raw GenTL Buffer module; note
            that you'll have to manipulate the object by yourself.

        cycle_s : float
            Set the cycle that defines how frequently check if a buffer is
            available. The unit is [s].

        Returns
        -------
        Union[Buffer, _Buffer, None]
            A buffer object if the resource is complete; otherwise None.
        """

        if self._event_new_buffer_thread and \
                self._event_new_buffer_thread.is_running():
            buffer = None
            while not buffer:
                buffer = self._try_fetch_from_queue(is_raw=is_raw)
                if not buffer:
                    time.sleep(cycle_s if cycle_s else 0.0001)
                else:
                    return buffer
        else:
            buffers = []
            for monitor in self._new_buffer_event_monitor_dict.values():
                buffer = None
                while not buffer:
                    try:
                        buffer = self._fetch(monitor=monitor,
                                             timeout_on_client_fetch_call=timeout,
                                             throw_except=True)
                    except GenTL_GenericException:
                        raise
                buffers.append(self._finalize_fetching_process(buffer, is_raw))
            return buffers if len(self._new_buffer_event_monitor_dict.values()) > 1 else buffers[0]

    def _update_num_images_to_acquire(self) -> None:
        if self._num_images_to_acquire >= 1:
            self._num_images_to_acquire -= 1

        if self._num_images_to_acquire == 0:
            self._emit_callbacks(self.Events.READY_TO_STOP_ACQUISITION)

    def _update_statistics(self, buffer) -> None:
        assert buffer
        self._statistics.increment_num_images()
        self._statistics.update_timestamp(buffer)

    def _create_raw_buffers(
            self, num_buffers: int = 0, size: int = 0) -> List[bytes]:
        assert num_buffers >= 0
        assert size >= 0

        raw_buffers = []
        for _ in range(num_buffers):
            _logger.debug(
                "allocated: {0} bytes by {1}".format(size, self))
            raw_buffers.append(bytes(size))

        return raw_buffers

    @staticmethod
    def _create_buffer_tokens(raw_buffers: List[bytes] = None):
        assert raw_buffers

        _buffer_tokens = []
        for i, buffer in enumerate(raw_buffers):
            _buffer_tokens.append(
                BufferToken(buffer, i))

        return _buffer_tokens

    def _announce_buffers(
            self, data_stream: DataStream = None,
            _buffer_tokens: List[BufferToken] = None) -> List[Buffer]:
        global _logger

        assert data_stream

        announced_buffers = []
        for token in _buffer_tokens:
            announced_buffer = data_stream.announce_buffer(token)
            announced_buffers.append(announced_buffer)
            _logger.debug(
                'announced: {0}'.format(_family_tree(announced_buffer)))

        return announced_buffers

    def _queue_announced_buffers(
            self, data_stream: Optional[DataStream] = None,
            buffers: Optional[List[Buffer]] = None) -> None:
        global _logger

        assert data_stream

        for buffer in buffers:
            data_stream.queue_buffer(buffer)
            _logger.debug('queued: {0}'.format(_family_tree(buffer)))

    def stop_image_acquisition(self):
        """
        Attention
        ---------
            It has been deprecated at 1.3.7 and will be removed at 2.0.0.
            It will be replaced by :meth:`stop`.
        """
        _indicate_deprecation(self.stop_image_acquisition, self.stop)
        self.stop_acquisition()

    def stop_acquisition(self) -> None:
        """
        Attention
        ---------
            It has been deprecated at 1.3.7 and will be removed at 2.0.0.
            It will be replaced by :meth:`stop`.
        """
        _indicate_deprecation(self.stop_acquisition, self.stop)
        self.stop()

    def stop(self) -> None:
        """
        Stops image acquisition process.
        """
        global _logger

        if self.is_acquiring():
            self._is_acquiring = False

            if self._event_new_buffer_thread.is_running():
                self._event_new_buffer_thread.stop()
                self._event_new_buffer_thread.join()

            with MutexLocker(self._event_new_buffer_thread):
                try:
                    self.remote_device.node_map.AcquisitionStop.execute()
                except GenApi_GenericException as e:
                    _logger.warning(e, exc_info=True)
                else:
                    _logger.debug('stopped streaming: {}'.format(
                        _family_tree(self._device_proxy.module)))

                try:
                    self.remote_device.node_map.TLParamsLocked.value = 0
                except GenApi_GenericException:
                    pass
                except AttributeError:
                    _logger.debug("no TLParamsLocked: {}".format(
                        _family_tree(self._device_proxy.module)))

                for data_stream in self._data_streams:
                    try:
                        data_stream.stop_acquisition(
                            ACQ_STOP_FLAGS_LIST.ACQ_STOP_FLAGS_KILL
                        )
                    except GenTL_GenericException as e:
                        _logger.warning(e, exc_info=True)

                    self._flush_buffers(data_stream)

                for monitor in self._new_buffer_event_monitor_dict.values():
                    monitor.flush_event_queue()

                if self._create_ds_at_connection:
                    self._release_buffers()
                else:
                    self._release_data_streams()

                for monitor in self._new_buffer_event_monitor_dict.values():
                    monitor.flush_event_queue()

            self._has_acquired_1st_image = False
            self._chunk_adapter.detach_buffer()
            _logger.info('stopped acquisition: {}'.format(self))

        if self._profiler:
            self._profiler.print_diff()

    def _flush_buffers(self, data_stream: DataStream) -> None:
        self._emit_callbacks(self.Events.RETURN_ALL_BORROWED_BUFFERS)
        data_stream.flush_buffer_queue(
            ACQ_QUEUE_TYPE_LIST.ACQ_QUEUE_ALL_DISCARD)
        _logger.debug('flushed: {0}'.format(data_stream))

    def _release_data_streams(self) -> None:
        global _logger

        self._release_buffers()

        for data_stream in self._data_streams:
            if data_stream and data_stream.is_open():
                name = _family_tree(data_stream.module)
                data_stream.close()
                _logger.debug('closed: {}'.format(name))

        self._data_streams.clear()
        self._new_buffer_event_monitor_dict.clear()

    def _release_buffers(self) -> None:
        global _logger

        for data_stream in self._data_streams:
            if data_stream.is_open():
                self._flush_buffers(data_stream)

        self._announced_buffers.clear()

        while not self._queue.empty():
            _ = self._queue.get_nowait()

