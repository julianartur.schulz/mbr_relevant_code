#include <iostream>
#include "VimbaCPP/Include/VimbaCPP.h"

#ifdef _VIMBA_CPP_HEADER_
    #define VIMBA_ACCESSIBLE 1
#else
    #define VIMBA_ACCESSIBLE 0
#endif

int main() {
    if (VIMBA_ACCESSIBLE) {
        std::cout << "VimbaCPP is accessible!" << std::endl;
    } else {
        std::cout << "VimbaCPP is not accessible." << std::endl;
    }
    return 0;
}
