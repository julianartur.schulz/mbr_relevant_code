gaussian_fit_range = 2
max_frames = 100000
N_window = 1000
khz_values = [1,5,10,25,50]

# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 13:17:46 2022

@author: BetzLab-Admin
"""

import sys
sys.path.append('../../lib')
from tqdm import tqdm
from scipy.signal import fftconvolve
import numpy as np
import os
import matplotlib.pyplot as plt
from fast_detection_methods import trajectory_gauss, trajectory_mean, trajectory_corr
from movie_processing import movie_to_array, clean_problem_pixels
from Powerspectra import powerspectrum, fit_power_spectrum, plot_power_spectra, fast_analytical_noisy_powerspectrum
#from pipython.datarectools import getservotime
import json
#core.set_exposure(0.5)##sets the cameras exposure time for snapshots to half a milisecond

def select_file(khz_value, files):
    for file in files:
        if f"{khz_value}_khz" in file:
            return file
    return None

data_path = r'\\134.76.13.158\betzlab\Julian\Hydrogel\measurement_2'

file_list =[ file for file in  os.listdir(data_path) if file.endswith('.avi')]
for khz_value in khz_values:
    file = select_file(khz_value, file_list)
    if file is None:
        print(f"Could not find file for {khz_value} kHz")
        continue
    print(f"Processing {file}")
    movie_path = os.path.join(data_path, file)
    numpy_movie = movie_to_array(movie_path, max_frames=max_frames)
    numpy_movie = clean_problem_pixels(numpy_movie)

    dt = 1 / (khz_value*1000)

    tra_mean = trajectory_mean(numpy_movie)
    tra_corr=trajectory_corr(numpy_movie)
    gauss_degrees = range(gaussian_fit_range)
    tra_gauss_list = []
    for degree in gauss_degrees:
        tra_gauss_list.append(trajectory_gauss(numpy_movie, degree=degree))

    trajectory_list = [tra_mean, tra_corr] + tra_gauss_list
    name_list = ['mean', 'corr'] + ['gauss_' + str(degree) for degree in gauss_degrees]

    x_trajecories = [trajectory[:,0] for trajectory in trajectory_list]
    y_trajecories = [trajectory[:,1] for trajectory in trajectory_list]
    # 1. Compute power spectra for each trajectory
    f_ks_list = []
    power_spectrum_list = []

    for trajectory in x_trajecories:
        f_ks, power_spectrum = powerspectrum(trajectory, dt)
        f_ks_list.append(f_ks)
        power_spectrum_list.append(power_spectrum)

    # 2. Fit the power spectra
    fit_results_list = []

    for f_ks, power_spectrum in zip(f_ks_list, power_spectrum_list):
        fit_result = fit_power_spectrum(f_ks, power_spectrum, bin_size = N_window)
        fit_results_list.append(fit_result)
    save_folder = os.path.join(data_path, f"{khz_value}_khz")
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)
    for i in range(len(trajectory_list)):
        folder = os.path.join(save_folder, name_list[i])
        if not os.path.exists(folder):
            os.makedirs(folder)
        np.save(os.path.join(folder, 'x_trajecories.npy'), x_trajecories[i])
        np.save(os.path.join(folder, 'y_trajecories.npy'), y_trajecories[i])
        np.save(os.path.join(folder, 'f_ks.npy'), f_ks_list[i])
        np.save(os.path.join(folder, 'power_spectrum.npy'), power_spectrum_list[i])
        with open(os.path.join(folder, 'fit_results.json'), 'w') as f:
            json.dump(fit_results_list[i], f, indent=4)
    plot_power_spectra(f_ks_list, power_spectrum_list, fit_results_list, name_list, smoothing_window = N_window)
    plt.savefig(os.path.join(save_folder, 'power_spectra.png'))
    plt.xscale('log')
    plt.savefig(os.path.join(save_folder, 'power_spectra_log.png'))


