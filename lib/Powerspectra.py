import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def powerspectrum(trajectory, dt, k_max=None):
    # Compute the FFT of the trajectory
    trajectory_fft = np.fft.fft(trajectory)
    if k_max != None:
        k_max = min(k_max, len(trajectory) // 2)
    else:
        k_max = len(trajectory) // 2

    f_ks = np.arange(1, k_max+1) /(len(trajectory) * dt)

    frequncy_indeces = np.arange(1, k_max+1)
    
    # Only consider up to k_max frequencies
    truncated_fft = trajectory_fft[frequncy_indeces]
    
    # Compute the power spectrum
    power_spectrum =dt* np.abs(truncated_fft)**2 /len(trajectory)
    

    
    return f_ks, power_spectrum

def bin_data(fk, ps, num_bins = 50):
    bin_edges = np.logspace(np.log10(fk[1]), np.log10(fk[-1]), num_bins)
    bin_indices = np.digitize(fk, bin_edges)
    bin_ps = [np.mean(ps[bin_indices == i]) for i in range(1, len(bin_edges))]
    bin_members = [np.sum(bin_indices == i) for i in range(1, len(bin_edges))]

    # For plotting purposes, we might want to use the geometric mean of the bin edges
    bin_centers = np.sqrt(bin_edges[:-1] * bin_edges[1:])
    return np.array(bin_centers), np.array(bin_ps),np.array(bin_members)


def fast_analytical_noisy_powerspectrum(f, dt, Kbt_over_gamma=1, f_c=1/(2*np.pi), noise=0, N_alisasing=10):
    f_nyquist = 1 / (2 * dt)
    n_values = np.arange(-N_alisasing, N_alisasing + 1)
    f_shifted = f[:, np.newaxis] + 2 * n_values * f_nyquist
    sinc_vals = (np.sinc(f_shifted*dt))**2
    denom = f_c**2 + f_shifted**2
    aliassed_sum = Kbt_over_gamma / (2 * np.pi**2) * (sinc_vals / denom).sum(axis=1)
    return aliassed_sum + noise**2 / (f_nyquist*2)


def fit_power_spectrum(f_ks, power_spectrum, num_bins=50, N_speedup=1, fix_f_c=False, log_space=False):
    # Bin power_spectrum
    f_k_binned, power_spectrum_binned, bin_members = bin_data(f_ks, power_spectrum, num_bins=num_bins)
    dt = (0.5/f_ks[-1])
    
    # Downsample for speedup
    f_k_binned = f_k_binned[::N_speedup]
    power_spectrum_binned = power_spectrum_binned[::N_speedup]
    sigma = [ps_binned/np.sqrt(bin_size) for ps_binned, bin_size in zip(power_spectrum_binned, bin_members)]
    
    # If log_space, transform the data
    if log_space:
        f_k_binned = np.log(f_k_binned)
        power_spectrum_binned = np.log(power_spectrum_binned)
        sigma = [np.log(s) for s in sigma]

    # Adjusted analytical function to assume dt
    def analytical_func(f_k_binned, Kbt_over_gamma, f_c, noise_param):
        ps = fast_analytical_noisy_powerspectrum(f_k_binned, dt, Kbt_over_gamma, f_c, noise_param)
        return np.log(ps) if log_space else ps

    # Initial guesses (without dt)
    p0 = [1, 0 if fix_f_c else 1/(2*np.pi), 0]

    # If fix_f_c, modify the function and initial guesses accordingly
    if fix_f_c:
        def analytical_func(f_k_binned, Kbt_over_gamma, noise_param):
            ps = fast_analytical_noisy_powerspectrum(f_k_binned, dt, Kbt_over_gamma, 0, noise_param)
            return np.log(ps) if log_space else ps
        p0 = [1, 0]

    # Fitting the function
    popt, pcov = curve_fit(analytical_func, f_k_binned, power_spectrum_binned, p0=p0, sigma=sigma, absolute_sigma=True)
    uncertainties = np.sqrt(np.diag(pcov))

    # Constructing the result dictionary
    result = {
        'dt': {'value': dt},
        'Kbt_over_gamma': {"value": popt[0], "uncertainty": uncertainties[0]}
    }
    
    if not fix_f_c:
        result['f_c'] = {"value": popt[1], "uncertainty": uncertainties[1]}
        result['noise_param'] = {"value": popt[2], "uncertainty": uncertainties[2]}
    else:
        result['f_c'] = {"value": 0, "uncertainty": 0}
        result['noise_param'] = {"value": popt[1], "uncertainty": uncertainties[1]}

    return result


def plot_power_spectra(f_ks_list, power_spectrum_list, fit_results_list, labels, N_plot_speedup=1, num_bins=50):
    plt.figure(figsize=(10, 10))
    colors = plt.cm.viridis(np.linspace(0, 1, len(labels))) # Color map
    
    # Plot original data
    for color, f_ks, ps, label in zip(colors, f_ks_list, power_spectrum_list, labels):
        plt.plot(f_ks[::N_plot_speedup], ps[::N_plot_speedup], alpha=0.3, label=f'Original {label}', color=color)
    
    # Plot binned data
    for color, f_ks, ps, label in zip(colors, f_ks_list, power_spectrum_list, labels):
        binned_f_ks, binned_ps, _ = bin_data(f_ks, ps, num_bins=num_bins)
        plt.plot(binned_f_ks[::N_plot_speedup], binned_ps[::N_plot_speedup], alpha=0.9, label=f'Smoothed {label}', color=color)
    
    # Plot analytical data
    for color, f_ks, fit_result, label in zip(colors, f_ks_list, fit_results_list, labels):
        analytical_ps = fast_analytical_noisy_powerspectrum(f_ks[::N_plot_speedup], 
                                                   dt=fit_result['dt']['value'], 
                                                   Kbt_over_gamma=fit_result['Kbt_over_gamma']['value'], 
                                                   f_c=fit_result['f_c']['value'], 
                                                   noise=fit_result['noise_param']['value'])
        plt.plot(f_ks[::N_plot_speedup], analytical_ps, '--', label=f'Analytical {label}', linewidth=5, color=color)
    
    plt.yscale('log')
    plt.xlabel('Frequency')
    plt.ylabel('Power spectrum')
    plt.legend()

def chi_square_function(params, binned_f_ks, binned_data, P, N_w, n_b):
    P_k = P(binned_f_ks, *params)
    chi2 = np.sum(n_b * N_w * ((binned_data / P_k) - 1)**2)
    return chi2

def fit_chi_square(f_ks, data, P, bin_size, p0):
    binned_f_ks, binned_data = bin_data(f_ks, data, bin_size)
    N_w = len(binned_data)
    n_b = bin_size

    popt, pcov = curve_fit(lambda f_ks, *params: chi_square_function(params, binned_f_ks, binned_data, P, N_w, n_b), binned_f_ks, binned_data, p0=p0)
    uncertainties = np.sqrt(np.diag(pcov))
    return popt, uncertainties
def fit_power_spectrum_chi_square(f_ks, power_spectrum, N_speedup=1, bin_size=50):

    dt = (0.5/f_ks[-1])

    # Downsample for speedup
    f_ks = f_ks[::N_speedup]
    power_spectrum = power_spectrum[::N_speedup]

    # Adjusted analytical function to assume dt
    def analytical_func(f_ks, Kbt_over_gamma, f_c, noise_param):
        return fast_analytical_noisy_powerspectrum(f_ks, dt, Kbt_over_gamma, f_c, noise_param)

    # Initial guesses (without dt)
    p0 = [1, 1/(2*np.pi), 0]

    # Chi-square fitting
    try:
        popt, pcov = fit_chi_square(f_ks, power_spectrum, analytical_func, bin_size, p0)
        uncertainties = np.sqrt(np.diag(pcov))
    except RuntimeError:
        popt = p0
        uncertainties = np.zeros(len(p0))
        print("Chi-square fitting failed. Using initial guesses instead.")


    # Constructing the result dictionary
    result = {
        'dt': {'value':dt},
        'Kbt_over_gamma': {"value": popt[0], "uncertainty": uncertainties[0]},
        'f_c': {"value": popt[1], "uncertainty": uncertainties[1]},
        'noise_param': {"value": popt[2], "uncertainty": uncertainties[2]}
    }

    return result
