
from pipython import pitools
import numpy as np
from pipython import GCSDevice, pitools
from pycromanager import Core,Studio
import json
import numpy as np
from scipy.ndimage import label, maximum_filter
from scipy.signal import fftconvolve
def snap_image():
    core=Core()
    # acquire an image and display it
    core.snap_image()
    tagged_image = core.get_tagged_image()
    # get the pixels in numpy array and reshape it according to its height and width
    image_array = np.reshape(
        tagged_image.pix,
        newshape=[-1, tagged_image.tags["Height"], tagged_image.tags["Width"]],
    )
    # return the first channel if multiple exists
    return image_array[0, :, :]

def position(pidevice):
    positions = pidevice.qPOS(pidevice.axes)
    return np.array([positions['1'],positions['2']])

def move_stage(pidevice,axis,target):
    rangemin = pidevice.qTMN()
    rangemax = pidevice.qTMX()
    if target >= rangemin[str(axis)] and target <= rangemax[str(axis)]:
        pidevice.MOV(axis, target)
        pitools.waitontarget(pidevice, axes=axis)
def move_large_stage(dx,dy):
    core=Core()
    if dx>200 or dy>200:
        print("movement of large stage too large")
        return
    xy_stage_device = core.get_xy_stage_device()
    current_x, current_y = core.get_x_position(xy_stage_device), core.get_y_position(xy_stage_device)
    core.set_xy_position(current_x+dx, current_y+dy)
def calibration_matrix(filename):
    with open(filename) as f:
        data = json.load(f)
    # Extract coefficients
    dx_coeff = [data['dx_camera']['coeff_dx_stage'], 
                data['dx_camera']['coeff_dy_stage']]
    
    dy_coeff = [data['dy_camera']['coeff_dx_stage'],
                data['dy_camera']['coeff_dy_stage']]
    
    # Create coefficient matrix
    cal_matrix = np.array([dx_coeff, dy_coeff])
    return cal_matrix

def center_crop(roi_size=100):
    core=Core()
    img=snap_image()
    (image_width,image_heigth)=img.shape
    
    x0=int(image_width/2-roi_size/2)
    y0=int(image_heigth/2-roi_size/2)
    
    
    ROI = [x0, y0, roi_size, roi_size]
    core.set_roi(*ROI)

def fullscreen(image_width=2304,image_heigth=2304):
    core=Core()
    
    #core.set_property("HamamatsuHam_DCAM", "Exposure", 10)
    core.set_roi(*[0,0,image_heigth,image_width])


###########centering functions #####################

def maximum_to_center(calibration_file='C:/Users/BetzLab-Admin/Desktop/git-code/mbr_relevant_code/callibrations/big_stage/big_stage_fixed_calibration_images/fit_parameters_refined.json'
):
    matrix=calibration_matrix(calibration_file)
    pixel_to_motor=np.linalg.inv(matrix[::-1, ::-1].T)
    img_0=snap_image()
    max_y, max_x = np.unravel_index(np.argmax(img_0), img_0.shape)
    position = np.array([max_x,max_y])
    center=np.array([img_0.shape[1]/2,img_0.shape[0]/2])
    vector_to_center=center-position
    motor_steps=pixel_to_motor@vector_to_center
    move_large_stage(motor_steps[1],motor_steps[0])
    img_1=snap_image()
    max_y, max_x = np.unravel_index(np.argmax(img_1), img_1.shape)
    position = np.array([max_x,max_y])
    vector_to_center=center-position
    motor_steps=pixel_to_motor@vector_to_center
    move_large_stage(motor_steps[1],motor_steps[0]*0.25)
    return

def modified_gaussian_kernel(sigma, truncate=4.0):
    size = int(truncate * sigma + 0.5) * 2 + 1
    y, x = np.mgrid[-size // 2 + 1:size // 2 + 1, -size // 2 + 1:size // 2 + 1]
    g = np.exp(-(x**2 + y**2) / (2.0 * sigma**2))
    g = g / g.sum()
    g = g - g.mean()
    return g

def convolve_with_kernel(image, kernel):
    return fftconvolve(image, kernel, mode='same')

def threshold_image(image, factor=0.1):
    return image > (np.max(image) * factor)

def get_local_maxima(image, size):
    return maximum_filter(image, size=size) == image

def get_labeled_peaks(image):
    return label(image)
def get_central_peak(labeled_peaks, num_peaks, image_shape):
    center = np.array(image_shape) / 2

    # Get all labeled peak coordinates
    peak_coords = np.column_stack(np.where(labeled_peaks > 0))
    
    # Get the center for each labeled peak
    peak_centers = np.array([peak_coords[labeled_peaks[peak_coords[:,0], peak_coords[:,1]] == i].mean(axis=0) for i in range(1, num_peaks+1)])
    
    # Compute the distance from the image center for each peak center
    distances = np.linalg.norm(peak_centers - center, axis=1)
    
    # Find the index of the peak with the smallest distance
    closest_peak_idx = np.argmin(distances)

    return tuple(peak_centers[closest_peak_idx])





def find_modified_gaussian_peak(image, sigma=16):
    kernel = modified_gaussian_kernel(sigma)
    convolved_image = convolve_with_kernel(image, kernel)
    peaks = threshold_image(convolved_image)
    local_max = get_local_maxima(convolved_image, sigma)
    peaks = peaks & local_max
    labeled_peaks, num_peaks = get_labeled_peaks(peaks)
    return get_central_peak(labeled_peaks, num_peaks, image.shape)

def beat_to_center(calibration_file='C:/Users/BetzLab-Admin/Desktop/git-code/mbr_relevant_code/callibrations/big_stage/big_stage_fixed_calibration_images/fit_parameters_refined.json'):
    matrix=calibration_matrix(calibration_file)
    pixel_to_motor=np.linalg.inv(matrix[::-1, ::-1].T)
    img_0=snap_image()
    max_y, max_x = find_modified_gaussian_peak(img_0)
    position = np.array([max_x,max_y])
    center=np.array([img_0.shape[1]/2,img_0.shape[0]/2])
    vector_to_center=center-position
    motor_steps=pixel_to_motor@vector_to_center
    move_large_stage(motor_steps[1],motor_steps[0])
    img_1=snap_image()
    max_y, max_x =find_modified_gaussian_peak(img_1)
    position = np.array([max_x,max_y])
    vector_to_center=center-position
    motor_steps=pixel_to_motor@vector_to_center
    move_large_stage(motor_steps[1],motor_steps[0]*0.25)
    return

def center_of_mass(img):
    rows, cols = np.indices(img.shape)
    total_weight = img.sum()

    if total_weight == 0:
        return img.shape[0] / 2, img.shape[1] / 2  # Return the geometric center

    com_x = np.sum(cols * img) / total_weight
    com_y = np.sum(rows * img) / total_weight
    
    return com_y, com_x

def center_object(max_attempts=30, threshold=1):
    # Load calibration matrix
    calibration_file = 'C:/Users/BetzLab-Admin/Desktop/git-code/mbr_relevant_code/callibrations/big_stage/big_stage_fixed_calibration_images/fit_parameters_refined.json'
    matrix = calibration_matrix(calibration_file)
    pixel_to_motor = np.linalg.inv(matrix[::-1, ::-1].T)
    
    attempts = 0
    
    while attempts < max_attempts:
        img = snap_image()
        com = center_of_mass(img)
        center = np.array([img.shape[1]/2, img.shape[0]/2])
        vector_to_center = center - com
        
        # Check the distance between com and center
        distance = np.linalg.norm(vector_to_center)
        if distance < threshold:
            return  # Return the centered image
        
        # Otherwise, continue with the protocol
        motor_steps = pixel_to_motor @ vector_to_center
        move_large_stage(motor_steps[0], motor_steps[1])
        
        attempts += 1
    
    # If we reach here, the object wasn't centered within the given attempts
    print("Warning: Object not centered after maximum attempts.")
    return




###########centering z position #####################
def move_z_direction(dz):
    core=Core()
    if dz>30:
        print("z movement of large stage too large")
        return
    current_z = core.get_position()
    core.set_position(current_z+dz)
def fit_parabola(x, y):
    # Fit y = ax^2 + bx + c
    A = np.vstack([x**2, x, np.ones_like(x)]).T
    a, b, c = np.linalg.lstsq(A, y, rcond=None)[0]
    return a, b, c
def median_intensity(image):
    return np.median(image)

def optimal_z_position():
    # Capture image at current z position and calculate its median intensity
    img_current = snap_image()
    median_current = median_intensity(img_current)
    
    # Move one step down, capture image, and measure its median intensity
    move_z_direction(-1)  # Assuming one step is -1 in your setup
    img_down = snap_image()
    median_down = median_intensity(img_down)
    
    # Move two steps up (so we end up one step from the starting position), capture image, and measure its median intensity
    move_z_direction(2)  # Move 2 steps up
    img_up = snap_image()
    median_up = median_intensity(img_up)
    
    # Now, we have 3 points: (-1, median_down), (0, median_current), and (1, median_up)
    x = np.array([-1, 0, 1])
    y = np.array([median_down, median_current, median_up])
    
    # Fit the points to a parabola
    a, b, c = fit_parabola(x, y)
    
    # Calculate the x-coordinate of the minimum of the parabola
    x_min = -b / (2*a)
    
    # If the minimum is less than 3 steps away from the current position, move to that position
    if abs(x_min) <= 3:
        move_z_direction(x_min)


