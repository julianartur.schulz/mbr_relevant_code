


import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares
from scipy.optimize import minimize
from scipy import signal
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy.optimize import Bounds

from scipy.fftpack import fft2, ifft2

from scipy.signal import correlate2d, fftconvolve
from numpy import flip, arange, reshape

from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter
#from skimage.feature import peak_local_max
#from skimage.transform import resize
import multiprocessing as mp


# def gaussian2D(xy, amplitude, x0, y0, sigma_x, sigma_y, theta):
#     x, y = xy
#     a = (np.cos(theta)**2) / (2 * sigma_x**2) + (np.sin(theta)**2) / (2 * sigma_y**2)
#     b = -(np.sin(2*theta)) / (4 * sigma_x**2) + (np.sin(2*theta)) / (4 * sigma_y**2)
#     c = (np.sin(theta)**2) / (2 * sigma_x**2) + (np.cos(theta)**2) / (2 * sigma_y**2)
#     g = amplitude * np.exp(-(a * ((x - x0)**2) + 2 * b * (x - x0) * (y - y0) + c * ((y - y0)**2)))
#     return g.ravel()

# def fit_2d_gaussian(image):
#     # Calculate the center of mass
#     com=ndimage.center_of_mass(image)
#     # Define the x and y coordinates
#     x = np.arange(image.shape[1])
#     y = np.arange(image.shape[0])
#     xx, yy = np.meshgrid(x, y)
#     xy = (xx, yy)
#     # Set initial guesses for the fit
#     amplitude_guess = np.max(image)
#     (y0_guess, x0_guess) = np.unravel_index(image.argmax(), image.shape)
#     sigma_x_guess = 50
#     sigma_y_guess = 50
#     theta_guess = np.min(image)
#     # Perform the fit
#     p0 = [amplitude_guess, x0_guess, y0_guess, sigma_x_guess, sigma_y_guess, theta_guess]
#     popt, pcov = curve_fit(gaussian2D, xy, image.ravel(), p0=p0,maxfev=10000)#,bounds=([-1,x0_guess-5,y0_guess-5,0,0,0],[np.max(image)*1.1,x0_guess+5,y0_guess+5,50,50,10]),maxfev=1000)
#     # Extract the x and y centers from the fit
#     x_center = popt[1]
#     y_center = popt[2]
#     return (x_center, y_center)

# from scipy.ndimage import gaussian_filter
# from skimage.feature import peak_local_max

# def fit_gaussian(image):
#     # Find the position of the maximum intensity
#     max_position = peak_local_max(image, num_peaks=1)[0]

#     # Define the size of the crop box around the maximum position
#     crop_size = 50

#     # Crop the image around the maximum position
#     cropped_image = image[max_position[0]-crop_size:max_position[0]+crop_size,
#                           max_position[1]-crop_size:max_position[1]+crop_size]

#     # Define the initial parameters for the Gaussian fit
#     initial_guess = (cropped_image.max(),
#                      crop_size,
#                      crop_size,
#                      cropped_image.min())

#     # Define the x and y coordinates of the cropped image
#     x, y = np.meshgrid(np.arange(2*crop_size), np.arange(2*crop_size))

#     # Perform the Gaussian fit
#     def gaussian(xy, amplitude, x0, y0, offset):
#         x, y = xy
#         g=amplitude * np.exp(-((x-x0)**2 + (y-y0)**2) / (2*(2)**2)) + offset
#         return g.ravel()

#     popt, _ = curve_fit(gaussian, (x, y), cropped_image.ravel(), p0=initial_guess)

#     # Calculate the position of the Gaussian center in coordinates of the uncropped image
#     x0 = max_position[1] - crop_size + popt[1]
#     y0 = max_position[0] - crop_size + popt[2]

#     return (x0, y0)

# def fit_cubic(image):
#     # Find the position of the maximum intensity
#     max_position = peak_local_max(image, num_peaks=1)[0]

#     # Define the size of the crop box around the maximum position
#     crop_size = 50

#     # Crop the image around the maximum position
#     cropped_image = image[max_position[0]-crop_size:max_position[0]+crop_size,
#                           max_position[1]-crop_size:max_position[1]+crop_size]
    
#     # Define the x and y coordinates of the cropped image
#     x, y = np.meshgrid(np.arange(2*crop_size), np.arange(2*crop_size))
    
import numpy as np
from scipy.interpolate import RectBivariateSpline
from scipy.optimize import minimize

def find_max_with_subpixel(array_2d, subpixel_res=2):
    # Find index of maximum value in the array
    idx_max = np.unravel_index(np.argmax(array_2d), array_2d.shape)

    # Define a region of interest (ROI) around the maximum
    roi_size = subpixel_res * 2
    roi_slices = tuple(slice(max(0, idx-roi_size), min(dim, idx+roi_size+1))
                       for idx, dim in zip(idx_max, array_2d.shape))
    roi = array_2d[roi_slices]
    # plt.figure('Roi')
    # plt.imshow(roi)

    # Create a bicubic interpolation function for the ROI
    x = np.arange(roi_slices[0].start, roi_slices[0].stop)
    y = np.arange(roi_slices[1].start, roi_slices[1].stop)
    interp_func = RectBivariateSpline(x, y, roi, kx=3, ky=3)

    # Define a function that returns the negative of the interpolated surface,
    # so that we can use a minimization algorithm to find the maximum
    def neg_interp_func(p):
        return -interp_func(p[0], p[1])[0]

    # Find the maximum of the fitted surface using a numerical optimization algorithm
    res = minimize(neg_interp_func, idx_max, method='Powell')

    # Return the coordinates of the maximum, including the subpixel offset
    return tuple(res.x)
    
    
    
def differences_between_frames(frame1,frame2,pixelsize=1):
    a=frame1
    b=frame2
    
    xx=np.shape(a)[0]-1

    yy=np.shape(a)[1]-1
    
    fft_correlation = fftconvolve(a,flip(b)) 
    # plt.figure()
    # plt.imshow(fft_correlation)
    (cx1,cy1)=find_max_with_subpixel(fft_correlation)
    cx1=cx1-xx
    cy1=cy1-yy
    
    cx1*=pixelsize
    cy1*=pixelsize
    
    return (cx1,cy1)


def getTrajectory(frames,pixelsize,rate):
    xx=np.shape(frames[0])[0]-1

    yy=np.shape(frames[0])[1]-1


    pos=[[0,0,0]]


    a=frames[0]
    for i in range(len(frames)-1):
        #print(i)
        #a=frames[i]
        b=frames[i+1]        
        (cy,cx) = differences_between_frames(a, b,pixelsize)
        #current_t,current_x,current_y=pos[-1]    
        #pos.append([i+1,cx+current_x,cy+current_y])
        pos.append([i+1,cx,cy])
        

       
    pos=np.asarray(pos)


    pos[:,0]=pos[:,0]/rate
    return (pos)

def getTrajectory_stepwise(frames,pixelsize,rate):
    xx=np.shape(frames[0])[0]-1

    yy=np.shape(frames[0])[1]-1


    pos=[[0,0,0]]


    #a=frames[0]
    for i in range(len(frames)-1):
        #print(i)
        a=frames[i]
        b=frames[i+1]        
        (dy,dx) = differences_between_frames(a, b,pixelsize)
        current_t,current_x,current_y=pos[-1]    
        pos.append([i+1,dx+current_x,dy+current_y])
        #pos.append([i+1,cx,cy])
        

       
    pos=np.asarray(pos)


    pos[:,0]=pos[:,0]/rate
    return (pos)


from multiprocessing import Pool
def getTrajectory_parallel(frames,pixelsize,rate):

    
    pos=[(0,0)]*len(frames)
    pos[0]=(0,0)
    a=frames[0]
    pool = Pool()
    time=[0]
    for i in range(len(frames)-1):
        
        time.append((i+1)/rate)
        
        b=frames[i+1]
        
        pos[i+1] = pool.apply_async(differences_between_frames, [a,b,pixelsize])
    for p in range(len(pos)-1):
        # Update key with the actual result:
        pos[p+1] = pos[p+1].get()
        print(p)
    pool.close()
    pool.join()
    
    pos=np.asarray(pos).T
    time=np.asarray(time).T
    
    p=np.empty((np.shape(pos)[1],3))
    p[:,1]=pos[0,:]
    p[:,2]=pos[1,:]
    p[:,0]=time
        
        

       
    # pos=np.asarray(pos)


    # pos[:,0]=pos[:,0]/rate
    return (p)








    
    




#%%
if __name__ =="__main__":
    folder=r"X:\till\Till\PhD Data\Data\2023_03_22_MDCK"
    file=r'b01'
    frames=np.load(folder+r'/'+file+r'.npy')
    
    
    pixelsize=31
    trim_factor=0.6
    radius=30
    #Smooth
    rate=1000
    
    pos=getTrajectory_parallel(frames, pixelsize, rate)
    

    outfile_trajectory=folder+r"/"+file+"_trajectory_cross_correlation.npy"
    #np.save(outfile_trajectory,pos)
    
    plt.figure()
    plt.plot(pos[:,0],pos[:,1]*-1,label='Gauss fit')
    plt.xlabel('Time [s]')
    plt.ylabel('Position [nm]')
    #plt.savefig(folder+r"/"+file+"_trajectory_cross_correlation_x.png")
    plt.figure()
    plt.plot(pos[:,0],pos[:,2]*-1,label='Gauss fit')
    plt.xlabel('Time [s]')
    plt.ylabel('Position [nm]')
   # plt.savefig(folder+r"/"+file+"_trajectory_cross_correlation_y.png")
    
