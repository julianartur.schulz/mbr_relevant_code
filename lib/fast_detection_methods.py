from concurrent.futures import ThreadPoolExecutor
import numpy as np
from collections import Counter
import logging
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import as_completed
import os 

from scipy.signal import fftconvolve
from scipy.optimize import curve_fit


logging.basicConfig(level=logging.INFO)
#Heler funciton used in multiple trackings
def most_common_value(movie, num_samples=10):
    # Randomly select num_samples frames from the movie
    sampled_frames = np.random.choice(movie.shape[0], size=num_samples, replace=False)
    
    # Extract these frames and flatten them into a single 1D array
    data = movie[sampled_frames].flatten()

    # Find and return the most common value
    counter = Counter(data)
    return counter.most_common(1)[0][0]


#Mean Trajectory

def position_finder_mean(image_matrix):
    I, J = image_matrix.shape
    total_signal = image_matrix.sum()
    
    mean_y = np.sum(np.arange(I)[:, None] * image_matrix) / total_signal
    mean_x = np.sum(np.arange(J) * image_matrix) / total_signal
    
    return [mean_y, mean_x]
def process_frame_parallel(frame_data):
    return position_finder_mean(frame_data)
def trajectory_mean(movie, logging_enabled=False, num_threads=None):
    if logging_enabled:
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.WARNING)

    ground_value = most_common_value(movie)
    movie -= ground_value

    # Parallel processing with progress bar
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        results = list(tqdm(executor.map(process_frame_parallel, movie), total=movie.shape[0], desc="Processing frames", unit="frame"))

    results = np.array(results)
    results = results - results[0]
    return results

# Correlation Method
def process_frame_corr(args):
    frame, movie, R ,fourier_filer = args
    image_1 = movie[frame]
    image_2 = movie[frame + 1]
    x_shift, y_shift, sigma_x, sigma_y = find_shift_corr(image_1, image_2, R, fourier_filer = fourier_filer)
    return x_shift, y_shift, sigma_x, sigma_y

def trajectory_corr(movie, R=None, fourier_filer = False):
    if R is None:
        R = find_ideal_R(movie)  # Assuming this function is defined elsewhere
    ground_value = most_common_value(movie)
    movie = movie - ground_value

    frames = movie.shape[0]
    y_traj = [0]
    x_traj = [0]
    sigma_y_traj = [0]
    sigma_x_traj = [0]

    with ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(process_frame_corr, [(frame, movie, R, fourier_filer) for frame in range(frames - 1)]), total=frames-1, desc="Processing frames", unit="frame"))

    for x_shift, y_shift, sigma_x, sigma_y in results:
        y_traj.append(y_traj[-1] + y_shift)
        x_traj.append(x_traj[-1] + x_shift)
        sigma_x_traj.append(sigma_x)
        sigma_y_traj.append(sigma_y)

    tray = np.array([x_traj, y_traj])
    sigma_tray = np.array([sigma_x_traj, sigma_y_traj])
    tray = -np.transpose(tray) * 2
    sigma_tray = np.transpose(sigma_tray) * 2
    return tray
def process_R_evaluation(args):
    k, movie, R_list = args
    image_1 = movie[k]
    image_2 = movie[k+1]
    fft_convolved_image = fftconvolve(image_1, np.flip(image_2), mode='same')
    error_list = []

    for R in R_list:
        try:
            peak, uncertainty = fit_gaussian(fft_convolved_image, R)
            error_list.append(np.linalg.norm(uncertainty))
        except:
            error_list.append(1)
    return R_list[np.argmin(error_list)]
def fourier_filter_image(image, band_width):
     # Perform 2D Fourier Transform
    f_transform = np.fft.fft2(image)

    # Shift the zero-frequency component to the center
    f_transform_centered = np.fft.fftshift(f_transform)

    # Copy for visualization later
    f_transform_centered_visual = np.copy(f_transform_centered)

    # Mask out the high-frequency oscillations due to even-odd row differences near the top and bottom
    rows, cols = image.shape

    replacement_value = np.median(f_transform_centered)

    f_transform_centered[:band_width, :] = replacement_value
    f_transform_centered[-band_width:, :] = replacement_value
    # Mark the areas put to 0 in the visualization copy with a red box
    f_transform_centered_visual[:band_width, :] = np.max(f_transform_centered_visual)
    f_transform_centered_visual[-band_width:, :] = np.max(f_transform_centered_visual)

    # Compute the magnitude spectrum for visualization
    magnitude_spectrum = np.log(np.abs(f_transform_centered_visual) + 1)

    # Shift back the zero-frequency component to the top-left corner
    f_transform_filtered = np.fft.ifftshift(f_transform_centered)

    # Perform Inverse 2D Fourier Transform to get the filtered image
    filtered_image = np.real(np.fft.ifft2(f_transform_filtered))

    return filtered_image
def find_ideal_R(movie):
    movie -= most_common_value(movie)
    num_samples = 10
    sample_interval = len(movie) // num_samples
    R_list = list(range(3, 50, 1))
    
    with ThreadPoolExecutor() as executor:
        R_recommendations = list(executor.map(process_R_evaluation, [(i * sample_interval, movie, R_list) for i in range(num_samples - 1)]))
    return Counter(R_recommendations).most_common(1)[0][0]
def find_shift_corr(image_1, image_2, R, fourier_filer = False):
    fft_convolved_image = fftconvolve(image_1, np.flip(image_2), mode='same')
    if fourier_filer:
        height, width = fft_convolved_image.shape
        fft_convolved_image = fourier_filter_image(fft_convolved_image, height//2-3)
    peak, uncertainty = fit_gaussian(fft_convolved_image, R)
    x_shift = peak[1] / 2 - image_1.shape[0] / 4
    y_shift = peak[0] / 2 - image_1.shape[1] / 4
    return x_shift, y_shift, uncertainty[0], uncertainty[1]

def fit_gaussian(data, R):
    y, x = np.indices(data.shape)
    x = x.ravel()
    y = y.ravel()
    z = data.ravel()
    
    # Initial guess for the parameters
    amp_guess = np.max(data)
    y0_guess, x0_guess = np.unravel_index(np.argmax(data), data.shape)
    sigma_guess, _ = estimate_fwhm_2d(data)
    sigma_guess /= 2.355  # Convert FWHM to sigma
    background_guess = 0
    initial_guess = (amp_guess, x0_guess, y0_guess, sigma_guess, background_guess)
    
    # Create a mask for data points within a radius R around the maximum
    mask = ((x - x0_guess)**2 + (y - y0_guess)**2) <= R**2
    x_masked = x[mask]
    y_masked = y[mask]
    z_masked = z[mask]
    
    try:
        popt, pcov = curve_fit(gaussian, (x_masked, y_masked), z_masked, p0=initial_guess)
        peak = (popt[1], popt[2])
        uncertainty = np.sqrt(np.diag(pcov))[[1, 2]]
    except Exception as e:
        print(f"Error during fitting: {e}")
        peak = (x0_guess, y0_guess)
        uncertainty = (1, 1)  # Arbitrary values indicating high uncertainty
    
    return peak, uncertainty
def estimate_fwhm_2d(data):
    # Find the maximum value and its index in the data
    max_idx = np.unravel_index(data.argmax(), data.shape)

    # Create 1D cuts along the x and y axes at the peak
    cut_x = data[max_idx[0], :]
    cut_y = data[:, max_idx[1]]

    # Estimate FWHM for each cut
    fwhm_x = estimate_fwhm_1d(cut_x)
    fwhm_y = estimate_fwhm_1d(cut_y)

    return fwhm_x, fwhm_y

def estimate_fwhm_1d(data):
    half_max = np.max(data) / 2.
    indices = np.where(data >= half_max)
    return indices[0][-1] - indices[0][0] + 1  # Add 1 to correct for endpoint
def gaussian(pos, amp, x0, y0, sigma,background):
    x,y = pos
    exponent = ((x - x0) ** 2 + (y - y0) ** 2) / (2 * sigma ** 2)
    return amp * np.exp(-exponent)+background

# Gaussian fit
def gaussian_2d(coords, amplitude, x0, y0, r0, sigma, background, *polynomial_params, degree=1):
    x, y = coords
    r = np.sqrt((x - x0) ** 2 + (y - y0) ** 2)
    gaussian_profile = amplitude * np.exp(-(r - r0) ** 2 / (2 * sigma ** 2))

    # Construct polynomial background based on the degree
    poly_value = polynomial_params[0]  # Start with the constant term
    idx = 1
    for i in range(1, degree+1):
        for j in range(i+1):
            poly_value += polynomial_params[idx] * (x**j) * (y**(i-j))
            idx += 1

    return gaussian_profile * poly_value + background

def get_initial_guess(image, degree=1):
    y, x = np.mgrid[:image.shape[0], :image.shape[1]]
    data = image.ravel()
    
    num_polynomial_params = (degree+1)*(degree+2)//2  # number of terms for the polynomial
    guess = np.zeros(6 + num_polynomial_params)
    
    # Gaussian parameters
    guess[0] = data.max()  # amplitude
    guess[2], guess[1] = np.unravel_index(data.argmax(), image.shape)  # x0, y0
    guess[3] = guess[4] = image.shape[0] / 8  # r0 and sigma
    guess[5] = np.median(data)  # background
    
    # Polynomial parameters (set to 0 by default)
    guess[6] =1 # constant part of polynomial
    for i in range(7, 5 + num_polynomial_params):
        guess[i] = 0

    return guess


def fit_rotated_gaussian(arr, degree=1, guess=None):
    y, x = np.mgrid[:arr.shape[0], :arr.shape[1]]
    data = arr.ravel()
    
    if guess is None:
        guess = get_initial_guess(arr, degree=degree)

    coords = np.vstack((x.ravel(), y.ravel()))
    try:
        popt, pcov = curve_fit(lambda coords, *params: gaussian_2d(coords, *params, degree=degree), coords, data, p0=guess, maxfev=10000)
    except Exception as e:
        #print(f"Error during fitting: {e}")
        popt = guess
        pcov = np.zeros((len(popt), len(popt)))
    return popt, np.sqrt(np.diag(pcov))


def find_position_gaussianfit(image,guess, degree=1):
    fit_params, fit_uncertanties= fit_rotated_gaussian(image, guess=guess, degree=degree)
    return fit_params, [fit_params[2], fit_params[1]]
def find_position_gaussianfit_parallel(frame_data, old_params, degree=1):
    # Perform the Gaussian fit
    fit_params, pos = find_position_gaussianfit(frame_data, old_params, degree=degree)
    pos_x = fit_params[2]
    pos_y = fit_params[1]
    return fit_params, [pos_x, pos_y]



def trajectory_gauss(movie, num_threads=None, degree=1):
    if not num_threads:
        num_threads = os.cpu_count() or 4

    ground_value = most_common_value(movie)
    movie -= ground_value

    tray = [None] * len(movie)  # Initialize the tray with None placeholders
    reference_params = {}
    step = 100

    # Process every 100th frame first with progress bar
    for i in tqdm(range(0, len(movie), step), desc="Processing every 100th frame"):
        fit_params, position = find_position_gaussianfit(movie[i], None, degree=degree)
        reference_params[i] = fit_params
        tray[i] = position

    # Fill in the frames between the reference points
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        futures = {}
        for i in tqdm(range(1, len(movie)), desc="Filling in frames"):
            if i % step == 0:
                continue  # We've already processed this frame
            # Find the nearest reference point
            nearest_ref = min(reference_params.keys(), key=lambda k: abs(k-i))
            future = executor.submit(find_position_gaussianfit_parallel, movie[i], reference_params[nearest_ref], degree=degree)
            futures[future] = i

        for future in tqdm(as_completed(futures), desc="Collecting results", total=len(futures)):
            fit_params, position = future.result()
            frame_index = futures[future]
            tray[frame_index] = position

    tray = [position for position in tray if position is not None]  # Remove any None placeholders

    return np.array(tray) - tray[0]
