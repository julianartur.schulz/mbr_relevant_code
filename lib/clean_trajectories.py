
import numpy as np
from Powerspectra import powerspectrum


def Laplace_NLL(params, x_data, y_data, function):
    y_model = function(x_data, *params)
    NLL = np.sum(y_data / y_model + np.log(y_model))
    return NLL


def fit_function(x, a, b, c):
    return a*x**b + c

def initial_guess(x_data, y_data):
    log_x_data = np.log(x_data)
    log_y_data = np.log(y_data)
    c = np.exp(log_y_data[-1])
    index_zero = np.argmin(np.abs(log_x_data))
    a = np.exp(log_y_data[index_zero]) - c
    b = (log_y_data[0] - log_y_data[index_zero] )/log_x_data[0]
    return np.array([a, b, c])

def fit_result(x_data, y_data, fit_function = fit_function, initial_guess = initial_guess):
    p_0 = initial_guess(x_data, y_data)
    result = minimize(Laplace_NLL, p_0, args=(x_data, y_data, fit_function), method='Nelder-Mead')
    return result

def get_suprise(x_data, y_data, fit_function = fit_function, initial_guess = initial_guess):
    result = fit_result(x_data, y_data, fit_function = fit_function, initial_guess = initial_guess)

    suprise = []
    for i in range(len(x_data)):
        NLL=Laplace_NLL(result.x, [x_data[i]], [y_data[i]], fit_function)
        expected_NLL = 1+np.log(fit_function(x_data[i], *result.x))
        suprise.append(NLL-expected_NLL)
    suprise = np.array(suprise)
    return suprise
def find_max_cumsum_interval(cumsum):
    """
    Find the indices i and j (where j > i) such that cumsum[j] - cumsum[i] is maximal.

    Args:
    cumsum (np.array): The cumulative sum array.

    Returns:
    tuple: A tuple (i, j) representing the indices.
    """
    argmin_cumsum = np.argmin(cumsum)
    argmax_cumsum = np.argmax(cumsum)

    if argmax_cumsum < argmin_cumsum:
        min_before_max = np.argmin(cumsum[:argmax_cumsum])
        max_before_min = np.argmax(cumsum[argmin_cumsum:])

        first_diff = cumsum[argmax_cumsum] - cumsum[min_before_max]
        second_diff = cumsum[max_before_min + argmin_cumsum] - cumsum[argmin_cumsum]

        if first_diff > second_diff:
            return (min_before_max, argmax_cumsum)
        else:
            return (argmin_cumsum, max_before_min + argmin_cumsum)
    else:
        return (argmin_cumsum, argmax_cumsum)


def delete_peak(x_data,y_data,fit_function = fit_function, initial_guess = initial_guess):
    suprise = get_suprise(x_data, y_data, fit_function, initial_guess)

    cumsum_sup = np.cumsum(suprise)
    (start_peak, end_peak) = find_max_cumsum_interval(cumsum_sup)

    exclude_indices = np.array(range(start_peak,end_peak))
    filtered_x_data = np.delete(x_data, exclude_indices)
    filtered_y_data = np.delete(y_data, exclude_indices)

    return filtered_x_data, filtered_y_data