
import numpy as np
import cv2
from tqdm import tqdm
from scipy.signal import convolve2d

def movie_to_array(video_path, channel=0, max_frames=None):
    """
    Convert a movie to a numpy array with the option to truncate the movie.
    """

    # Open the video file
    cap = cv2.VideoCapture(video_path)

    # Check if the video file was opened successfully
    if not cap.isOpened():
        print("Error: Couldn't open the video file.")
        return None

    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_slices = []

    if max_frames is not None:
        total_frames = min(total_frames, max_frames)

    # Add tqdm progress bar
    test_image = cap.read()[1]
    threshold = 0#most_common_value(test_image[2:, :, channel].flatten())+3

    for _ in tqdm(range(total_frames), desc="Processing video", unit="frame"):
        ret, frame = cap.read()
        if not ret:
            break
        image = frame[2:, :, channel]
        image[image<threshold] = threshold
        image = image - threshold

        frame_slices.append(image)

    cap.release()

    # Adjust the dimensions so that movie[0] is the first frame
    return np.array(frame_slices).astype(np.int32)
def replace_with_neighbour_average(movie, pixel_positions, overwrite=False):
    """
    Replace the value of specified pixels in every frame with the average of their 8 neighbours.

    Parameters:
    - movie: A 3D numpy array where each 2D slice is a frame of the movie.
    - pixel_positions: A list of (x, y) positions to be replaced.

    Returns:
    - A modified movie.
    """
    if overwrite:
        modified_movie = movie
    else:
        modified_movie = movie.copy()

    # Define the neighbouring offsets
    neighbour_offsets = [(-1, -1), (-1, 0), (-1, 1),
                         ( 0, -1),          ( 0, 1),
                         ( 1, -1), ( 1, 0), ( 1, 1)]

    for frame in modified_movie:
        for x, y in pixel_positions:
            neighbour_values = []
            for dx, dy in neighbour_offsets:
                nx, ny = x + dx, y + dy
                
                # Check boundaries
                if 0 <= nx < frame.shape[0] and 0 <= ny < frame.shape[1]:
                    neighbour_values.append(frame[nx, ny])
            
            # Replace the pixel value with the average of its neighbours
            frame[x, y] = np.mean(neighbour_values)
    
    return modified_movie
def find_high_response_pixels(movie, num_frames=10, threshold=100):
    # Define the Laplace filter
    filter = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
    
    # Randomly select 10 frames from the movie
    selected_indices = np.random.choice(len(movie), num_frames, replace=False)
    selected_frames = movie[selected_indices]
    
    pixel_counts = np.zeros(movie[0].shape, dtype=int)
    
    for frame in selected_frames:
        # Convolute the frame with the Laplace filter
        convoluted_image = convolve2d(frame, filter, mode='same', boundary='symm')
        
        # Identify pixels exceeding the threshold
        high_response_pixels = np.where(convoluted_image > threshold)
        
        # Increment the count for each high-response pixel
        for i, j in zip(*high_response_pixels):
            pixel_counts[i, j] += 1
    
    # Find the pixels that exceed the threshold in most of the frames
    frequently_high_pixels = np.where(pixel_counts > num_frames * 0.8)
    
    return list(zip(frequently_high_pixels[0], frequently_high_pixels[1]))

def clean_problem_pixels(movie, num_frames=10, threshold=100, overwrite=False):
    """
    Find pixels that are likely to be probe particles and replace them with the average of their neighbours.
    """
    # Find pixels that are likely to be probe particles
    probe_pixels = find_high_response_pixels(movie, num_frames, threshold)
    
    # Replace the probe pixels with the average of their neighbours
    return replace_with_neighbour_average(movie, probe_pixels, overwrite= overwrite)


