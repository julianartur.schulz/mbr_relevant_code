import numpy as np
import os
from concurrent.futures import ThreadPoolExecutor
from tqdm import tqdm


def bin_data(data, bin_count):
    bin_indices = np.linspace(0, len(data), bin_count + 1, dtype=int)
    return np.add.reduceat(data, bin_indices[:-1]) / (bin_indices[1:] - bin_indices[:-1])

def b_distribution(segment, dt, Tao):
    backward_sampling = int(Tao / dt)
    rng = np.random.default_rng()
    random_samples = rng.choice(len(segment)-backward_sampling-1, size=min(100, len(segment)-backward_sampling-1), replace=False)
    b_mbr_samples = [segment[s] - segment[s - backward_sampling] for s in random_samples]    
    sigma = np.std(b_mbr_samples)
    return sigma

def calculate_segment_contribution(args):
    segment, dt, Tao, timespan, multiplier, max_mbr_points = args
    forward_sampling = int(timespan / dt)
    backward_sampling = int(Tao / dt)
    
    mbr_trajectory = np.zeros(min(forward_sampling, max_mbr_points))
    
    # Ensuring we don't go out of bounds
    sample_starts = np.arange(backward_sampling + 1, len(segment) - forward_sampling - 1)
 
    sigma = b_distribution(segment, dt, Tao)
    b_cutoff = sigma * multiplier
    n_valid_points =0
    for start in sample_starts:
        x_0_mbr = segment[start]
        #print(x_0_mbr)
        #assert x_0_mbr is float or x_0_mbr is int, f" x_0_mbr is {type(x_0_mbr)}"
        
        d_mbr = x_0_mbr - segment[start - backward_sampling]
        
        if abs(d_mbr) > b_cutoff:
            sample = segment[start : start + forward_sampling]
            sample_to_add = (-(sample - x_0_mbr) / d_mbr) * 1 / len(sample_starts)
            if forward_sampling > max_mbr_points:
                sample_to_add = bin_data(sample_to_add, max_mbr_points)
            mbr_trajectory += sample_to_add
            n_valid_points += 1    
    return mbr_trajectory*(len(sample_starts)/n_valid_points)

def mean_back_realxation(dt, trajectory, Tao, timespan, multiplier=1, num_threads=None, max_segments = None, max_mbr_points = 1000):
    if num_threads is None:
        num_threads = os.cpu_count()

    forward_sampling = int(timespan / dt)
    backward_sampling = int(Tao / dt)
    if max_segments is None:
        max_segments = forward_sampling 
    segments = []
    segment_size = min(len(trajectory) // num_threads,max_segments)
    print(f"Segment size: {segment_size}")
    for i in range(0, len(trajectory), segment_size):
        start = max(i - backward_sampling, 0)
        end = min(i + segment_size + forward_sampling, len(trajectory))
        # If this isn't the last segment, ensure the end doesn't overlap with the start of the next segment
        if i + segment_size+ forward_sampling < len(trajectory):
            segments.append(trajectory[start:end])
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        args = [(segment, dt, Tao, timespan, multiplier, max_mbr_points) for segment in segments]
        results = list(tqdm(executor.map(calculate_segment_contribution, args), total=len(segments), desc="Processing segments", position=0, leave=True))
    mbr_trajectory = np.mean(results, axis=0)
    return mbr_trajectory

def analytical_mean_back_realxation(dt,time,k,Diffusion_particle,Diffusion_oscillator):
    time_list=np.linspace(0,time,int(time/dt))
    prefactor=1/2*(1-Diffusion_oscillator/Diffusion_particle)
    analytical_mbr=[prefactor*(1-np.exp(-k*t)) for t in time_list]
    return analytical_mbr
