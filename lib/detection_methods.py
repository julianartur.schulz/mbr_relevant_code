import numpy as np
from matplotlib import pyplot as plt
import cv2
from collections import Counter
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from scipy.ndimage import shift
from scipy.signal import fftconvolve
from numpy.linalg import norm
from scipy.optimize import curve_fit
import from_Till as from_Till
import os
import logging
import pickle
from tqdm import tqdm

def most_common_value(values):
    counter = Counter(values)
    return counter.most_common(1)[0][0]

def position_finder_mean(image_matrix):
     mean_x=0
     mean_y=0
     I,J=image_matrix.shape
     for i in range(I):
          for j in range(J):
               mean_y+=i*image_matrix[i,j]
               mean_x+=j*image_matrix[i,j]
     total_signal=sum(sum(image_matrix))
     mean_y=mean_y/total_signal
     mean_x=mean_x/total_signal
     return [mean_y,mean_x]
def trajectory_mean(movie, logging_enabled=False):
    if logging_enabled:
        logging.basicConfig(level=logging.INFO)

    data = movie.flatten()
    ground_value = most_common_value(data)
    movie = movie - ground_value

    tray = []
    frames = movie.shape[0]

    # Only show progress bar if logging is enabled
    frame_range = tqdm(range(frames), desc="Processing frames", unit="frame") if logging_enabled else range(frames)

    for frame in frame_range:
        mean_y, mean_x = position_finder_mean(movie[frame])
        tray.append([mean_y, mean_x])

        # Log every 100 frames if logging is enabled
        if logging_enabled and frame % 100 == 0:
            logging.info(f"Processed frame {frame}/{frames}")

    tray = np.array(tray)
    tray = tray - tray[0]
    return tray


def gaussian(pos, amp, x0, y0, sigma,background):
    x,y = pos
    exponent = ((x - x0) ** 2 + (y - y0) ** 2) / (2 * sigma ** 2)
    return amp * np.exp(-exponent)+background


def fit_gaussian(data, R):
    y, x = np.indices(data.shape)
    x = x.ravel()
    y = y.ravel()
    z = data.ravel()

    # Initial guess for the parameters
    amp_guess = np.max(data)
    y0_guess, x0_guess = np.unravel_index(np.argmax(data), data.shape)
    sigma_guess, _ = estimate_fwhm_2d(data)  # Assuming this function is defined elsewhere
    background_guess=0

    initial_guess = (amp_guess, x0_guess, y0_guess, sigma_guess, background_guess)

    # Create a mask for data points within a radius R around the maximum
    mask = ((x - x0_guess)**2 + (y - y0_guess)**2) <= R**2

    # Apply the mask to the data
    x_masked = x[mask]
    y_masked = y[mask]
    z_masked = z[mask]

    try:
        # Fit the Gaussian
        popt, pcov = curve_fit(gaussian, (x_masked, y_masked), z_masked, p0=initial_guess)

        # Calculate the peak position and uncertainty
        amp, x0, y0, sigma, background = popt
        peak = (x0, y0)
        uncertainty = np.sqrt(np.diag(pcov))[[1, 2]]
    
    except Exception as e:
        print(f"Error during fitting: {e}")
        # Return the initial guess values if fit fails
        peak = (0, 0)
        uncertainty = (1, 1)  # No uncertainties if we're using the initial guess

    return peak, uncertainty

def estimate_fwhm_2d(data):
    # Find the maximum value and its index in the data
    max_val = data.max()
    max_idx = np.unravel_index(data.argmax(), data.shape)

    # Create 1D cuts along the x and y axes at the peak
    cut_x = data[max_idx[0], :]
    cut_y = data[:, max_idx[1]]

    # Estimate FWHM for each cut
    fwhm_x = estimate_fwhm_1d(cut_x)
    fwhm_y = estimate_fwhm_1d(cut_y)

    return fwhm_x, fwhm_y

def estimate_fwhm_1d(data):
    half_max = np.max(data) / 2.
    indices = np.where(data >= half_max)
    return indices[0][-1] - indices[0][0] + 1  # Add 1 to correct for endpoint

def find_shift_corr(image_1,image_2,R):
    fft_convolved_image=fftconvolve(image_1,np.flip(image_2),mode='same')
    #fft_convolved_image=fft_convolved_image-boundary_average(fft_convolved_image)
    peak, uncertainty=fit_gaussian(fft_convolved_image,R)
    x_shift=peak[1]/2-np.shape(image_1)[0]/4
    y_shift=peak[0]/2-np.shape(image_1)[1]/4
    return x_shift, y_shift, uncertainty[0],uncertainty[1]
from concurrent.futures import ThreadPoolExecutor
from tqdm import tqdm

def process_frame(args):
    frame, movie, R = args
    image_1 = movie[frame]
    image_2 = movie[frame + 1]
    x_shift, y_shift, sigma_x, sigma_y = find_shift_corr(image_1, image_2, R)
    return x_shift, y_shift, sigma_x, sigma_y

def trajectory_corr(movie, R=None):
    if R is None:
        R = find_ideal_R(movie)
    data = movie.flatten()
    ground_value = most_common_value(data)
    movie = movie - ground_value
    
    y_traj = [0]
    x_traj = [0]
    sigma_y_traj = [0]
    sigma_x_traj = [0]
    frames = movie.shape[0]

    with ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(process_frame, [(frame, movie, R) for frame in range(frames - 1)]), total=frames-1))

    for x_shift, y_shift, sigma_x, sigma_y in results:
        y_traj.append(y_traj[-1] + y_shift)
        x_traj.append(x_traj[-1] + x_shift)
        sigma_x_traj.append(sigma_x)
        sigma_y_traj.append(sigma_y)

    tray = np.array([x_traj, y_traj])
    sigma_teay = np.array([sigma_x_traj, sigma_y_traj])
    tray = -np.transpose(tray) * 2
    sigma_teay = np.transpose(sigma_teay) * 2
    return tray

def trajectory_till(movie):
    data=movie.flatten()
    ground_value=most_common_value(data)
    movie=movie-ground_value
    calc_traj_till=from_Till.getTrajectory(movie,1,1)
    calc_traj_till=-np.flip(calc_traj_till[:,1:3])
    calc_traj_till=np.flip(calc_traj_till, axis=0)
    return calc_traj_till

def trajectory_till_stepwise(movie):
    data=movie.flatten()
    ground_value=most_common_value(data)
    movie=movie-ground_value
    calc_traj_till=from_Till.getTrajectory_stepwise(movie,1,1)
    calc_traj_till=-np.flip(calc_traj_till[:,1:3])
    calc_traj_till=np.flip(calc_traj_till, axis=0)
    return calc_traj_till

def gaussian_2d(coords, amplitude, x0, y0, r0,sigma, background, slope_x, slope_y, x_intercept, y_intercept):
    x, y = coords
    x0, y0 = float(x0), float(y0)
    
    r=np.sqrt((x-x0)**2+(y-y0)**2)

    gaussian_profile=amplitude * np.exp(-(r-r0)**2/(2*sigma**2))

    
    return gaussian_profile*((x-x_intercept)*slope_x+(y-y_intercept)*slope_y)+ background

def fit_rotated_gaussian(arr, guess=None):

    x = np.linspace(0, arr.shape[1]-1, arr.shape[1])
    y = np.linspace(0, arr.shape[0]-1, arr.shape[0])
    x, y = np.meshgrid(x, y)
    
    # Flatten the data for curve fitting
    coords = np.vstack((x.flatten(), y.flatten()))
    data = arr.flatten()

    # Initial guess for the Gaussian parameters
    if type(guess)!=np.ndarray:
        amplitude_guess = data.max()
        x0_guess, y0_guess = np.unravel_index(np.argmax(data), arr.shape)
        #y0_guess, x0_guess=position_finder_mean(arr)
        sigma_guess= arr.shape[1] / 4
        r0_guess=arr.shape[1] / 4
        background_guess= np.median(data)
        slope_x_guess=0
        slope_y_guess=0
        x_intercept_guess=x0_guess
        y_intercept_guess=y0_guess

        guess = [amplitude_guess, x0_guess, y0_guess, r0_guess, sigma_guess, background_guess,slope_x_guess,slope_y_guess,x_intercept_guess,y_intercept_guess]


    # Fit the Gaussian
    popt, pcov  = curve_fit(gaussian_2d, coords, data, p0=guess,maxfev=10000)
    return popt, np.sqrt(np.diag(pcov))
def find_position_gaussianfit(image,guess):
    fit_params, fit_uncertanties= fit_rotated_gaussian(image, guess)
    return fit_params#fit_params[2], fit_params[1]

def trajectory_gauss(movie):
     data=movie.flatten()
     ground_value=most_common_value(data)
     movie=movie-ground_value
     tray=[]
     frames=movie.shape[0]
     old_params=None
     for frame in range(frames):
          #print(frame)
          fit_params=find_position_gaussianfit(movie[frame],old_params)
          pos_x=fit_params[2]
          pos_y=fit_params[1]
          old_params=fit_params
          #print(old_params)
          tray.append([pos_x,pos_y])
     tray=np.array(tray)
     tray=tray-tray[0]
     return tray

def move_image(image, x_dist, y_dist):
    return shift(image, [y_dist, x_dist], mode='wrap', order=1)

def bin_indices(data, num_bins):
    # Create bins
    hist, bin_edges = np.histogram(data, bins=num_bins)
    
    # Get bin indices for each value
    bin_indices = np.digitize(data, bin_edges)

    # Group values by bin index
    binned_indices = {}
    for i, bin_idx in enumerate(bin_indices):
        if bin_idx not in binned_indices:
            binned_indices[bin_idx] = []
        binned_indices[bin_idx].append(i)
    
    return binned_indices

def make_test_trajectory(image,min_step,max_step,picture_noise,timesteps):
    # Generate random angles between 0 and 2*pi
    angles = np.random.uniform(low=0, high=2*np.pi, size=(timesteps, 1))

    # Generate random norms between 0 and 1
    if max_step==0:
        norms=np.zeros((timesteps, 1))
    else:
        norms = np.random.uniform(low=np.log(min_step), high=np.log(max_step), size=(timesteps, 1))
        norms=np.exp(norms)

    # Create an Nx2 numpy array with each row having a random norm between 0 and 1
    space_steps = np.hstack((norms * np.cos(angles), norms * np.sin(angles)))

    movie=[image]
    place=[0,0]
    trajectory=[place]
    for step in space_steps:
        place=place+step
        trajectory.append(place)
        movie.append(move_image(image,place[1],place[0])+np.random.normal(0,picture_noise,image.shape))
    return np.array(trajectory),np.array(movie)
def compare_trajectories(tray_test,tray_ground_truth):
    step_sizes=[]
    procentual_accuracy=[]
    for i in range(len(tray_ground_truth)-1):
        step_truth=tray_ground_truth[i+1]-tray_ground_truth[i]
        step_calc=tray_test[i+1]-tray_test[i]
        step_sizes.append(norm(step_truth))
        procentual_accuracy.append(norm(step_truth-step_calc)/norm(step_truth))
    
    Stepsize_bins=[]
    stepsize_bin_accuracies=[]
    sigma_stepsize_bin_accuracies=[]
    num_bins = 10
    #return step_sizes
    binned_indices = bin_indices(np.log(step_sizes), num_bins)
    step_sizes=np.array(step_sizes)
    procentual_accuracy=np.array(procentual_accuracy)
    for bin_idx, indices in binned_indices.items():
        #return indices
        Stepsize_bins.append(np.mean(step_sizes[indices]))
        stepsize_bin_accuracies.append(np.mean(procentual_accuracy[indices]))
        sigma_stepsize_bin_accuracies.append(np.std(procentual_accuracy[indices]))
    return Stepsize_bins, stepsize_bin_accuracies, sigma_stepsize_bin_accuracies

def compare_trajectory_method(method_function, image, min_step=0.0001, max_step=1, picture_noise=0.1 , timesteps=1000 , timesteps_per_batch=100):
    
    num_batches = timesteps // timesteps_per_batch
    final_calc_traj = np.empty((0, 2))
    final_test_traj = np.empty((0, 2))

    for i in range(num_batches):
        test_traj, test_movie = make_test_trajectory(image, min_step, max_step, picture_noise, timesteps_per_batch)
        test_traj = np.array(test_traj)
        calc_traj = np.array(method_function(test_movie))

        if i > 0:
            calc_prev_position = final_calc_traj[-1, :]
            calc_traj = calc_prev_position + calc_traj
            calc_traj=calc_traj[1:, :]

            test_prev_position = final_test_traj[-1, :]
            test_traj = test_prev_position + test_traj
            test_traj=test_traj[1:, :]


        final_calc_traj = np.concatenate((final_calc_traj, calc_traj), axis=0)
        final_test_traj = np.concatenate((final_test_traj, test_traj), axis=0)

    Stepsize_bins, stepsize_bin_accuracies, sigma_stepsize_bin_accuracies = compare_trajectories(final_calc_traj, final_test_traj)
    results = {
        "Stepsize_bins": Stepsize_bins,
        "stepsize_bin_accuracies": stepsize_bin_accuracies,
        "sigma_stepsize_bin_accuracies": sigma_stepsize_bin_accuracies,
        "test_traj": final_test_traj,
        "calc_traj": final_calc_traj,
        "picture_noise": picture_noise,
        "image": image,
        "name": f'{method_function.__name__}_noise_{picture_noise}'
    }
    return results

import math as mt

def xy_to_rp(x, y):
    r = mt.sqrt(x**2 + y**2)
    phi = mt.atan2(y, x)
    return r, phi
def trajectory_polar_comparison(results, name, size_scale=100):
    real_traj = results["test_traj"]
    calc_traj = results["calc_traj"]
    steps=[]
    calc_steps=[]
    for i in range(len(real_traj)-1):
        steps.append(real_traj[i+1]-real_traj[i])
        calc_steps.append(calc_traj[i+1]-calc_traj[i])
    steps_polar = [xy_to_rp(s[0],s[1]) for s in steps]
    calc_steps_polar = [xy_to_rp(s[0],s[1]) for s in calc_steps]
    steps_polar=np.array(steps_polar)
    calc_steps_polar=np.array(calc_steps_polar)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,5))
    fig.suptitle(name) # Set the title of the plot

    # Calculate the size of the points for the second subplot
    step_sizes = np.sqrt(steps_polar[:,0]**2 + steps_polar[:,1]**2)
    step_sizes /= np.max(step_sizes)  # normalize sizes to range [0,1]
    point_sizes = step_sizes * size_scale  # scale sizes

    # Plot the first scatter plot on the left subplot
    ax1.scatter(steps_polar[:,0], calc_steps_polar[:,0])
    ax1.axline((0, 0), slope=1, color='red', linestyle='--')
    ax1.set_xlabel('real')
    ax1.set_ylabel('predicted')
    ax1.set_title('Radius')
    ax1.set_xscale('log')
    ax1.set_yscale('log')

    # Plot the second scatter plot on the right subplot
    ax2.scatter(steps_polar[:,1], calc_steps_polar[:,1], s=point_sizes)  # use calculated sizes
    ax2.axline((0, 0), slope=1, color='red', linestyle='--')
    ax2.set_xlabel('real')
    ax2.set_ylabel('predicted')
    ax2.set_title('angle')

    # Adjust the spacing between subplots and display the figure
    fig.subplots_adjust(wspace=0.3)
    return steps_polar, calc_steps_polar

def plt_accuracy_results(results, label, color):
    Stepsize_bins = results["Stepsize_bins"]
    stepsize_bin_accuracies = results["stepsize_bin_accuracies"]
    sigma_stepsize_bin_accuracies = results["sigma_stepsize_bin_accuracies"]

    # Sort the data based on Stepsize_bins
    sorted_indices = np.argsort(Stepsize_bins, kind='mergesort')
    Stepsize_bins = np.array(Stepsize_bins)[sorted_indices]
    stepsize_bin_accuracies = np.array(stepsize_bin_accuracies)[sorted_indices]
    sigma_stepsize_bin_accuracies = np.array(sigma_stepsize_bin_accuracies)[sorted_indices]

    plt.plot(Stepsize_bins, stepsize_bin_accuracies, color=color, label=label)
    plt.errorbar(Stepsize_bins, stepsize_bin_accuracies, sigma_stepsize_bin_accuracies, ls='none', color=color)


def save_result(name, location, results):
    os.makedirs(location, exist_ok=True)
    path = os.path.join(location, f'{name}.pkl')

    with open(path, 'wb') as f:
        pickle.dump(results, f)

def load_result(name, location):
    path = os.path.join(location, f'{name}.pkl')
    with open(path, 'rb') as f:
        result = pickle.load(f)

    return result

def find_ideal_R(movie, plot=False):
    movie=movie-most_common_value(movie.flatten())
    random_indices = np.random.choice(len(movie)-1, 10, replace=False)
    R_list = list(range(3, 50, 1))
    R_recommendations = []

    if plot:
        plt.figure()
    for color, k in zip(plt.cm.viridis(np.linspace(0, 1, 10)), random_indices):
        image_1 = movie[k]
        image_2 = movie[k+1]
        fft_convolved_image = fftconvolve(image_1, np.flip(image_2), mode='same')
        peak, uncertainty = fit_gaussian(fft_convolved_image, 20)  # Assuming you have the fit_gaussian function defined elsewhere
        error_list = []

        for R in R_list:
            data = fft_convolved_image
            y, x = np.indices(data.shape)
            x = x.ravel()
            y = y.ravel()
            z = data.ravel()
            amp_guess = np.max(data)
            y0_guess, x0_guess = np.unravel_index(np.argmax(data), data.shape)
            sigma_guess, _ = estimate_fwhm_2d(data)
            background_guess = 0
            initial_guess = (amp_guess, x0_guess, y0_guess, sigma_guess, background_guess)
            mask = ((x - x0_guess)**2 + (y - y0_guess)**2) <= R**2
            x_masked = x[mask]
            y_masked = y[mask]
            z_masked = z[mask]

            try:
                popt, pcov = curve_fit(gaussian, (x_masked, y_masked), z_masked, p0=initial_guess)
                uncertainty = np.sqrt(np.diag(pcov))[[1, 2]]
                error_list.append(np.linalg.norm(uncertainty))
            except:
                error_list.append(1)

        R_recommendations.append(R_list[np.argmin(error_list)])
        if plot:
            plt.plot(R_list, error_list, label=f'Index {k}', color=color)

    if plot:
        plt.ylim([0, .06])
        plt.xlabel('R Value')
        plt.ylabel('Error')
        plt.title('Error vs R Value for Random Indices')
        plt.legend()
        plt.show()

    return most_common_value(R_recommendations)